#ifndef APU_H
#define APU_H

#include "memory_maps.h"
#include <SDL.h>

struct Triangle_t
{
    union
    {
        uint8_t linearValue;
        struct
        {
            uint8_t linear : 7;
            uint8_t halt : 1;
        };
    };

    uint8_t timerLow;

    union
    {
        uint8_t lengthValue;
        struct
        {
            uint8_t timerHigh : 3;
            uint8_t length : 5;
        };
    };

public:
    uint16_t timer() const
    {
        return ( timerHigh << 8 ) | ( timerLow );
    }
};

struct Pulse_t
{
    union
    {
        uint16_t statusValue;
        struct
        {
            uint16_t duty : 2;
            uint16_t loop : 1;
            uint16_t constant : 1;
            uint16_t volume : 4;
        } status;
    };

    union
    {
        uint16_t sweepValue;
        struct
        {
            uint16_t sweep : 1;
            uint16_t period : 3;
            uint16_t negate : 1;
            uint16_t shift : 3;
        } sweep;
    };

    uint8_t timerLow;

    union
    {
        uint16_t lengthValue;
        struct
        {
            uint16_t length : 5;
            uint16_t timerHigh : 3;
        } length;
    };

public:
    uint16_t timer() const
    {
        return ( length.timerHigh << 8 ) | ( timerLow );
    }
};

class APU
{
public:
    bool Init();
    void Run( uint16_t numCycles );
    void Shutdown();

    void Pause( bool pause = true );

    void AudioCallback( Uint8 *stream, int len );

private:
    uint16_t TranslateAddress( uint16_t addr )
    {
        // Pulse 1 mirrors
        {
            if ( addr == 0x4004 )
                return 0x4000;

            if ( addr == 0x4005 )
                return 0x4001;

            if ( addr == 0x4006 )
                return 0x4002;

            if ( addr == 0x4007 )
                return 0x4003;
        }

        return addr;
    }

public:
    void WriteByte( uint16_t addr, uint8_t data )
    {
        uint16_t tAddr = TranslateAddress( addr );

        if ( tAddr == MEM_APU_ChannelEnable )
        {
            m_ChannelStates.value = ( data & 0x1F );
            return;
        }

        if ( tAddr == MEM_APU_Pulse1 )
        {
            m_pulse1.statusValue = data;
            return;
        }

        if ( tAddr == MEM_APU_Pulse1 + 1 )
        {
            m_pulse1.sweepValue = data;
            return;
        }

        if ( tAddr == MEM_APU_Pulse1 + 2 )
        {
            m_pulse1.timerLow = data;
            return;
        }

        if ( tAddr == MEM_APU_Pulse1 + 3 )
        {
            m_pulse1.lengthValue = data;
            return;
        }

        if ( tAddr == MEM_APU_Triangle )
        {
            m_triangle.linearValue = data;
        }

        // Unused
        if ( tAddr == MEM_APU_Triangle + 1 )
            return;

        if ( tAddr == MEM_APU_Triangle + 2 )
        {
            m_triangle.timerLow = data;
        }

        if ( tAddr == MEM_APU_Triangle + 3 )
        {
            m_triangle.lengthValue = data;
        }
    }

private:
    void UpdatePulse( const Pulse_t &pulse );

    Pulse_t m_pulse1;
    Pulse_t m_pulse2;

private:
    void UpdateTriangle();

    Triangle_t m_triangle;

    //
    //
    //
private:
    struct
    {
        union
        {
            uint8_t value;
            struct
            {
                uint8_t square1 : 1;
                uint8_t square2 : 1;
                uint8_t triangle : 1;
                uint8_t noise : 1;
                uint8_t DMC : 1;
            };
        };

    } m_ChannelStates;

private:
    SDL_AudioDeviceID m_deviceID;
    double m_sample = 0.0;

    //
    // Mixer
    //
private:
    inline static const uint16_t k_Num_Samples = 1024;
    typedef float SampleBuffer_t[k_Num_Samples];
    SampleBuffer_t m_Samples_Pulse1;
    SampleBuffer_t m_Samples_Pulse2;
    SampleBuffer_t m_Samples_Triangle;
    SampleBuffer_t m_Samples_Noise;
    SampleBuffer_t m_Samples_DMC;

    inline static const uint32_t k_CPU_Rate = 17897731; // Hz

    enum class WaveType
    {
        Sine, // FIXME: Unused!
        Triangle,
        Saw, // FIXME: Unused!
        Square,
        Noise
    };

    void GenerateSamples( float *stream, int len, double frequency, WaveType source );

private:
    uint16_t m_cycleRemainder = 0;
};

#endif // APU_H