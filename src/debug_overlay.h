#ifndef DEBUG_OVERLAY_H
#define DEBUG_OVERLAY_H

struct SDL_Texture;

class DebugOverlay
{
public:
    ~DebugOverlay();

    struct FrameCapture_t
    {
        void reset()
        {
            valid = false;
            totalTime = 0.0;
            CPUTime = 0.0;
            APUTime = 0.0;
            PPUTime = 0.0;
            renderTime = 0.0;
        }

        bool valid = false;
        double totalTime = 0.0;
        double CPUTime = 0.0;
        double APUTime = 0.0;
        double PPUTime = 0.0;
        double renderTime = 0.0;
    };

    void Init();
    void RenderGraph( const FrameCapture_t &newCapture );

private:
    static const int k_Captures_Size = 100;
    DebugOverlay::FrameCapture_t m_Captures[k_Captures_Size];
    int m_CurrentCapture = 0;

public:
    SDL_Texture *GetOverlay() const
    {
        return m_overlay;
    }

private:
    SDL_Texture *m_overlay = nullptr;
    uint8_t *m_pixels = nullptr;
    int m_pitch = 0;
};

#endif // DEBUG_OVERLAY_H