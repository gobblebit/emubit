#ifndef SHIFT_REGISTER_H
#define SHIFT_REGISTER_H

#include <limits>

// TODO: Restrict to integral types!
template <typename T>
class ShiftRegister
{
public:
    T get( uint8_t signalBit = 0 ) const
    {
        return ( value >> signalBit );
    }

    bool isFull( uint8_t signalBit )
    {
        return ( value & ( 1 << ( signalBit - 1 ) ) );
    }

    void reset()
    {
        value = 0;
        value |= ( 1 << ( numBits - 1 ) );
    }

    void addBit( bool bitState )
    {
        value >>= 1;
        if ( bitState )
        {
            value |= ( 1 << ( numBits - 1 ) );
        }
        else
        {
            value &= ~( 1 << ( numBits - 1 ) );
        }
    }

private:
    T value = 0;
    const uint8_t numBits = std::numeric_limits<T>::digits;
};

#endif // SHIFT_REGISTER_H