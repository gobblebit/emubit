#ifndef TESTING_H
#define TESTING_H

#include <functional>

enum class TestState_t
{
    Ready,
    Running,
    Failed,
    Succeeded
};

struct Test_t
{
    const char *ROM_Filename;
    uint16_t entryPoint = MEM_Reset_Vector;
    std::function<TestState_t( void )> testFn;
};

class TestHarness
{
public:
    bool Init();
    TestState_t Update();

    void ProgressToNextTest()
    {
        ++m_currentTest;
    }

    bool Completed() const
    {
        return ( m_currentTest == m_Tests.size() );
    }

    uint16_t GetEntryPoint() const
    {
        ASSERT( !Completed() );
        return m_Tests[m_currentTest].entryPoint;
    }

    const char *GetErrorText() const
    {
        return m_errorText;
    }

    const char *GetROMFilename() const
    {
        ASSERT( !Completed() );
        return m_Tests[m_currentTest].ROM_Filename;
    }

private:
    int m_currentTest = 0;
    char m_errorText[256];
    std::vector<Test_t> m_Tests;
};

#endif // TESTING_H