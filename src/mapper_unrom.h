
#ifndef MAPPER_UNROM_H
#define MAPPER_UNROM_H

class Mapper_UNROM : public Mapper_NROM
{
public:
    virtual void OnPRGLoaded() override
    {
        m_numBanks = static_cast<uint8_t>( m_PRGSize >> 14 );
    }

    virtual void PRG_WriteByte( uint16_t addr, uint8_t data ) override
    {
        m_bank = ( data & 0x0F );
    }

    virtual uint8_t PRG_ReadByte( uint16_t addr ) override
    {
        if ( addr < 0xC000 )
            return m_PRG[( addr & ~0x8000 ) + ( m_bank << 14 )];

        return m_PRG[( addr - 0xC000 ) + ( ( m_numBanks - 1 ) << 14 )];
    }

protected:
    uint8_t m_numBanks = 0;
    uint8_t m_bank = 0;
};

#endif // MAPPER_UNROM_H