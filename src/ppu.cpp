/*
    PPU Emulator

    - 261 scanlines per frame
    - Scanline = 341 PPU cycles (or 113.667 CPU cycles)
    - Each clock cycle is one pixel
*/

#include "base.h"

#include <SDL.h>
#include "thirdparty/nes_ntsc/nes_ntsc.h"

#include "timer.h"

#include "ppu.h"

extern bool gUseNTSCFilter;
uint8_t gImage[gNativeWidth * gNativeHeight];
nes_ntsc_setup_t gSetup = nes_ntsc_composite;

Color_t k_Palettes[64];

const bool gEmulateSpriteLimit = true;

//
// Helper function for MMC3 scanline counter
// Called when the 12th bit in the VRAM register goes from low to high (ie. 0x0000 to 0x1000 )
//

void _PPU_A12_Changed( bool newState )
{
    gMapper->SignalA12Changed( newState );
}

uint16_t _PPU_GetCycle()
{
    return gPPU->GetCurrentCycle();
}

uint16_t _PPU_GetScanline()
{
    return gPPU->GetCurrentScanline();
}


//
//
//

PPU::PPU() : m_Memory(), m_OAM(), m_backgroundMask()
{
    // NTSC emulation filter
    m_NTSC = (nes_ntsc_t *)malloc( sizeof( nes_ntsc_t ) );
    nes_ntsc_setup_t gSetup = nes_ntsc_composite;
    nes_ntsc_init( m_NTSC, &gSetup );

    // Clear our internal memory
    memset( m_Memory, 0, ( 8 * 1024 ) );
    memset( m_OAM, 0, sizeof( m_OAM[0] ) * 64 );
    memset( m_backgroundMask, 0, ARRAYSIZE( m_backgroundMask ) );

    // Setup VRAM registers
    m_VRAM.V.value = 0x0000;
    m_VRAM.T.value = 0x0000;
    m_VRAM.fineX = 0;
    m_VRAM.W = false;

    // Allocate our SDL textures
    auto pixelFormat = SDL_PIXELFORMAT_RGB888;
    const uint16_t width = 1024;
    m_framebuffer = SDL_CreateTexture( gRenderer, pixelFormat, SDL_TEXTUREACCESS_STREAMING, width, 256 );
    m_CHRPage = SDL_CreateTexture( gRenderer, pixelFormat, SDL_TEXTUREACCESS_TARGET, 256, 256 );

    // Read in our palette definition
    std::ifstream file( "nes.pal", std::ios::in | std::ios::binary );
    if ( !file.is_open() )
        return;

    size_t curPaletteIndex = 0;
    while ( !file.eof() )
    {
        Color_t color;
        if ( file.read( (char *)&color, sizeof( color ) ) )
        {
            k_Palettes[curPaletteIndex++] = std::move( color );
        }
    }

    file.close();
}


//
//
//

PPU::~PPU()
{
    free( m_NTSC );

    SDL_UnlockTexture( m_framebuffer );
    SDL_DestroyTexture( m_framebuffer );

    SDL_UnlockTexture( m_CHRPage );
    SDL_DestroyTexture( m_CHRPage );
}


//
//
//

void PPU::UpdatePaletteIndices( PaletteType type )
{
    uint16_t paletteBase = ( type == PaletteType::Nametable ) ? MEM_PPU_Palette_Image : MEM_PPU_Palette_Sprites;
    uint8_t( *paletteIndices )[4][4] = ( type == PaletteType::Nametable ) ? &m_paletteIndices_Nametable : &m_paletteIndices_Sprites;

    const uint8_t backgroundIndex = ReadByte( paletteBase );
    ASSERT( backgroundIndex < ARRAYSIZE( k_Palettes ) );

    for ( int i = 0; i < 4; ++i )
    {
        // NOTE: Always slam the background!
        ( *paletteIndices )[i][0] = backgroundIndex;

        for ( int j = 1; j < 4; ++j )
        {
            const uint16_t addr = ( i * 4 ) + paletteBase + j;
            uint8_t index = ReadByte( addr );

            if ( IsGrayscaleMode() )
            {
                index &= 0x30;
            }

            ASSERT( index < ARRAYSIZE( k_Palettes ) );

            ( *paletteIndices )[i][j] = index;
        }
    }
}


//
//
//

void PPU::RenderNameTable( const uint16_t scanline, const uint16_t currentPixel )
{
    uint8_t stride = 4;
    uint8_t *writePos;

    if ( gUseNTSCFilter )
    {
        writePos = gImage + currentPixel + ( scanline * gNativeWidth );
    }
    else
    {
        writePos = m_pixels + ( currentPixel * stride ) + ( scanline * m_pitch );
    }

    uint8_t *maskWrite = m_backgroundMask + currentPixel + ( scanline * gNativeWidth );

    // If the background is hidden, clear the mask and write the raw background color
    if ( IsBackgroundHidden() )
    {
        if ( currentPixel < gNativeWidth )
        {
            *( maskWrite ) = 0;
            const Color_t &finalColor = k_Palettes[m_paletteIndices_Nametable[0][0]];
            if ( gUseNTSCFilter )
            {
                *( writePos ) = m_paletteIndices_Nametable[0][0];
            }
            else
            {
                *( writePos++ ) = finalColor.b;
                *( writePos++ ) = finalColor.g;
                *( writePos++ ) = finalColor.r;
            }
        }

        return;
    }

    const VRegister_t &v = m_VRAM.V;
    const uint8_t fineX = ( m_VRAM.fineX + currentPixel ) % 8;

    // Do the render
    const uint16_t tileAddress = GetTileAddress();
    const uint8_t tileIndex = ReadByte( 0x2000 | ( v.value & 0x0FFF ) );
    const uint8_t attribute = ReadByte( 0x23C0 | ( v.value & 0x0C00 ) | ( ( v.value >> 4 ) & 0x38 ) | ( ( v.value >> 2 ) & 0x07 ) );

    // +------------+------------+
    // |  Square 0  |  Square 1  |  #0-F represents an 8x8 tile
    // |   #0  #1   |   #4  #5   |
    // |   #2  #3   |   #6  #7   |  Square [x] represents four (4) 8x8 tiles
    // +------------+------------+   (i.e. a 16x16 pixel grid)
    // |  Square 2  |  Square 3  |
    // |   #8  #9   |   #C  #D   |
    // |   #A  #B   |   #E  #F   |
    // +------------+------------+

    // 33221100
    // ||||||+--- Upper two (2) color bits for Square 0 (Tiles #0,1,2,3)
    // ||||+----- Upper two (2) color bits for Square 1 (Tiles #4,5,6,7)
    // ||+------- Upper two (2) color bits for Square 2 (Tiles #8,9,A,B)
    // +--------- Upper two (2) color bits for Square 3 (Tiles #C,D,E,F)

    const uint8_t square = ( ( v.X >> 1 ) % 2 ) + ( ( ( v.Y >> 1 ) % 2 ) * 2 );
    const uint8_t paletteIndex = ( attribute >> ( square << 1 ) ) & 0x03;

    // Plane look-up
    // TODO: pattern_page | (tile << 4) | ((ppu_addr >> 12) & 7)
    const uint16_t plane0Addr = tileAddress + ( tileIndex << 4 ) + v.fineY;
    const uint8_t plane0 = ReadByte( plane0Addr );
    const uint8_t plane1 = ReadByte( plane0Addr + 8 );

    // NOTE: We need to be able to make these reads due to timing and expectations of
    //       certain mappers (like MMC2)
    if ( currentPixel >= gNativeWidth )
        return;

    // Bitplane composition
    const bool bit0 = !!( plane0 & ( 1 << ( 7 - fineX ) ) );
    const bool bit1 = !!( plane1 & ( 1 << ( 7 - fineX ) ) );
    uint8_t color = ( ( (uint8_t)bit1 << 1 ) | (uint8_t)bit0 ) & 0x03;
    Color_t finalColor = k_Palettes[m_paletteIndices_Nametable[paletteIndex][color]];

    // Mask left-most column
    if ( !IsLeftColumnBackgroundEnabled() && currentPixel < 8 )
    {
        color = 0;
        finalColor = k_Palettes[m_paletteIndices_Nametable[0][0]];
    }

    // Record the background mask at this position
    *( maskWrite ) = ( color == 0 ) ? 0 : 255;
    if ( gUseNTSCFilter )
    {
        // FIXME: This doesn't quite account for the left-column blanking!
        *( writePos ) = m_paletteIndices_Nametable[paletteIndex][color];
    }
    else
    {
        *( writePos++ ) = finalColor.b;
        *( writePos++ ) = finalColor.g;
        *( writePos++ ) = finalColor.r;
    }
}


//
//
//

void PPU::SelectSprites( const uint16_t scanline )
{
    m_numSelectedSprites = 0;

    // Seek all sprites for valid candidates on the coming scanline
    const uint8_t height = UsingTallSprites() ? 16 : 8;

    m_isSpriteZero = false;

    for ( int i = 0; i < 64; ++i )
    {
        const OAMEntry_t &sprite = m_OAM[i];

        if ( sprite.ypos >= scanline || sprite.ypos + height < scanline )
            continue;

        m_SelectedSprites[m_numSelectedSprites++] = sprite;

        if ( i == 0 )
        {
            m_isSpriteZero = true;
        }

        // Stop at the 8 sprite limit for now
        if ( gEmulateSpriteLimit && m_numSelectedSprites == 8 )
        {
            SetSpriteOverflow();
            return;
        }
    }
}


//
//
//

void PPU::RenderSprites( const uint16_t scanline, const uint16_t currentPixel )
{
    if ( currentPixel >= gNativeWidth )
        return;

    // FIXME: This is a hack!
    _PPU_A12_Changed( GetSpriteTileAddress() == 0x1000 );

    if ( IsSpritesHidden() )
    {
        SelectSprites( scanline );
        return;
    }

    const uint8_t height = UsingTallSprites() ? 16 : 8;

    // Now render each one
    for ( int i = 0; i < (int)m_numSelectedSprites; ++i )
    {
        const OAMEntry_t &sprite = m_SelectedSprites[i];

        // Cull sprites not at our current position
        if ( sprite.xpos + 8 < currentPixel || sprite.xpos > currentPixel )
            continue;

        const bool isSpriteZero = ( i == 0 ) && m_isSpriteZero;

        const uint8_t y = ( scanline - 1 ) - sprite.ypos;
        const uint16_t tallAddr = (uint16_t)sprite.tileIndex.bank << 12;
        const uint16_t baseAddress = ( UsingTallSprites() ) ? tallAddr : GetSpriteTileAddress();

        // Render out the sprites scanline in full
        {
            uint8_t tileIndex = sprite.tileIndex.index;

            if ( UsingTallSprites() )
            {
                tileIndex &= 0xFE;

                uint16_t tileOffset = 0;
                if ( y >= 8 )
                {
                    tileOffset ^= 0x01;
                }

                if ( sprite.attributes.flipVert )
                {
                    tileOffset ^= 0x01;
                }

                tileIndex += tileOffset;
            }

            const uint16_t yLookup = ( sprite.attributes.flipVert ) ? ( height - y - 1 ) : y;
            const uint16_t plane0Addr = baseAddress + ( tileIndex * 16 ) + ( yLookup % 8 );
            const uint8_t plane0 = ReadByte( plane0Addr );
            const uint8_t plane1 = ReadByte( plane0Addr + 8 );
            const uint16_t dY = sprite.ypos + y + 1;

            const uint8_t x = static_cast<uint8_t>( currentPixel - sprite.xpos );
            {
                const uint16_t dX = currentPixel;
                if ( !IsLeftColumnSpriteEnabled() && ( dX < 8 ) )
                    continue;

                const uint8_t bitOffset = ( sprite.attributes.flipHorz ) ? 7 - x : x;
                const bool bit0 = !!( plane0 & ( 1 << ( 7 - bitOffset ) ) );
                const bool bit1 = !!( plane1 & ( 1 << ( 7 - bitOffset ) ) );
                const uint8_t color = ( ( (uint8_t)bit1 << 1 ) | (uint8_t)bit0 ) & 0x03;

                // Don't render the pixel
                if ( color == 0 )
                    continue;

                // Zero sprite check
                if ( isSpriteZero )
                {
                    // Hardware misses at x=255
                    if ( dX < 255 )
                    {
                        // Background must be "opaque"
                        if ( m_backgroundMask[dX + ( dY * gNativeWidth )] )
                        {
                            FlagZeroSpriteHit();
                        }
                    }
                }

                // Test for background masking
                if ( sprite.attributes.priority )
                {
                    if ( m_backgroundMask[dX + ( dY * gNativeWidth )] )
                        break;
                }

                const Color_t finalColor = k_Palettes[m_paletteIndices_Sprites[sprite.attributes.palette][color]];

                if ( gUseNTSCFilter )
                {
                    *( gImage + ( dX + ( dY * gNativeWidth ) ) ) = m_paletteIndices_Sprites[sprite.attributes.palette][color];
                }
                else
                {
                    uint8_t *writePos = m_pixels + ( dX * 4 ) + ( dY * m_pitch );
                    *( writePos++ ) = finalColor.b;
                    *( writePos++ ) = finalColor.g;
                    *( writePos++ ) = finalColor.r;
                }
            }
        }
    }

    // Evaluate sprites for the next scanline
    // FIXME: Realistically this needs to occur over frame 65-256, filling the buffer as it goes.
    if ( currentPixel == 0 )
    {
        SelectSprites( scanline );
    }
}


//
//
//

SDL_Texture *PPU::GetCHRPage()
{
    uint16_t base[2] = { 0x0000u, 0x1000u };

    for ( int page = 0; page < 2; ++page )
    {
        const uint16_t numTiles = 256;
        for ( int tileset = 0; tileset < 2; ++tileset )
        {
            SDL_SetRenderTarget( gRenderer, m_CHRPage );
            const uint16_t baseAddress = base[page];

            for ( int i = 0; i < numTiles; ++i )
            {
                for ( int y = 0; y < 8; ++y )
                {
                    for ( int x = 0; x < 8; ++x )
                    {
                        const uint8_t bit0 = ReadByte( baseAddress + ( i * 16 ) + y ) & ( 1 << ( 7 - x ) );
                        const uint8_t bit1 = ReadByte( baseAddress + ( i * 16 ) + ( y + 8 ) ) & ( 1 << ( 7 - x ) );
                        const uint16_t dX = ( ( i % 16 ) * 8 ) + x + ( page * 128 );
                        const uint16_t dY = ( ( i / 16 ) * 8 ) + y;
                        uint8_t color = 0;
                        if ( bit0 )
                        {
                            color |= ( 1 << 0 );
                        }

                        if ( bit1 )
                        {
                            color |= ( 1 << 1 );
                        }

                        color <<= 6; // FIXME: Remove!
                        SDL_SetRenderDrawColor( gRenderer, color, color, color, 255 );
                        SDL_RenderDrawPoint( gRenderer, dX, dY );
                    }
                }
            }
        }
    }

    SDL_SetRenderTarget( gRenderer, NULL );
    return m_CHRPage;
}


//
//
//

void PPU::Run( uint8_t numCycles )
{
    Timer timer;

    // Update our palette for this cycle if they've been changed by the CPU
    if ( m_refreshPalette_Nametable )
    {
        UpdatePaletteIndices( PaletteType::Nametable );
        m_refreshPalette_Nametable = false;
    }

    if ( m_refreshPalette_Sprites )
    {
        UpdatePaletteIndices( PaletteType::Sprites );
        m_refreshPalette_Sprites = false;
    }

    for ( int i = 0; i < numCycles; ++i, AdvanceCycle() )
    {
        const uint16_t currentScanline = m_frameCycles / k_CyclesPerScanline;
        const uint16_t currentPixel = m_frameCycles % k_CyclesPerScanline;

        // Lock the texture for rendering when our current pass starts
        if ( currentScanline == 0 && currentPixel == 0 )
        {
            SDL_LockTexture( m_framebuffer, nullptr, reinterpret_cast<void **>( &m_pixels ), &m_pitch );
            ++m_frameNumber;

            // Skip this cycle if we're on an odd frame
            // FIXME: Is this correct any more?
            if ( !IsBackgroundHidden() && m_frameNumber % 2 )
                continue;
        }

        // Render the current scanline
        if ( currentScanline < k_ScanlinesBeforeVBlank )
        {
            // Scanline 240 is an idle one
            if ( currentScanline == 240 )
                continue;

            // Render
            RenderNameTable( currentScanline, currentPixel );
            RenderSprites( currentScanline, currentPixel );

            m_SignaledNMI = false;
            continue;
        }

        //
        // VBlank
        //

        if ( currentScanline == 241 && currentPixel == 1 )
        {
            // Special case behavior to emulate hardware issue
            const int diff = m_frameCycles - m_lastStatusReadCycle;
            if ( diff == 0 )
                continue;

            // Mark us as in the VBlank portion of the frame
            SetVBlank();

            // No more writes can occur
            SDL_UnlockTexture( m_framebuffer );

            // Call the NMI on the frame (unless disabled)
            if ( !IsNMIDisabled() && !m_SignaledNMI && IsVBlankFlagged() )
            {
                // _Log( "Signaled NMI at (%d, %d)\n", currentPixel, currentScanline );
                gCPU->SignalNMIPending();
                m_SignaledNMI = true;
            }
        }

        // Final scanline (dummy)
        if ( currentScanline == 261 )
        {
            // Clear VBL
            if ( currentPixel == 1 )
            {
                ClearZeroSpriteHit();
                ClearSpriteOverflow();
                ClearVBlank();
            }

            if ( IsRendering() )
            {
                if ( currentPixel >= 280 && currentPixel <= 304 )
                {
                    // Take vertical components
                    m_VRAM.V.fineY = m_VRAM.T.fineY;
                    m_VRAM.V.Y = m_VRAM.T.Y;

                    // Catch A12 changing for MMC3 IRQ counter
                    if ( ( m_VRAM.T.value & 0x0800 ) != ( m_VRAM.V.value & 0x0800 ) )
                    {
                        _PPU_A12_Changed( !!( m_VRAM.T.value & 0x0800 ) );
                    }

                    if ( m_VRAM.T.value & 0x0800 )
                    {
                        m_VRAM.V.value |= 0x0800;
                    }
                    else
                    {
                        m_VRAM.V.value &= ~( 0x0800 );
                    }
                }
            }
        }
    }

    timer.Stop();
    m_duration = timer.GetDuration();
}


//
//
//

void PPU::RenderNTSCFilter()
{
    static int burstPhase = 0;

    burstPhase ^= 1;
    if ( gSetup.merge_fields )
        burstPhase = 0;

    nes_ntsc_blit( m_NTSC, gImage, 256, burstPhase, gNativeWidth, gNativeHeight, m_pixels, m_pitch );
}
