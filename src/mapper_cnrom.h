#ifndef MAPPER_CNROM_H
#define MAPPER_CNROM_H

class Mapper_CNROM : public Mapper_NROM
{
public:
    virtual void PRG_WriteByte( uint16_t addr, uint8_t data ) override
    {
        m_bank = ( data & 0x03 );
    }

    virtual void CHR_WriteByte( uint16_t addr, uint8_t data ) override
    {
        const bool isROM = true;
        ASSERT( !isROM );
    }

    virtual uint8_t CHR_ReadByte( uint16_t addr ) override
    {
        const uint32_t bankOffset = ( addr < 0x2000 ) ? ( m_bank * 0x2000 ) : 0x00;
        return m_CHR[addr + bankOffset];
    }

protected:
    uint16_t m_bank = 0;
};

#endif // MAPPER_CNROM_H