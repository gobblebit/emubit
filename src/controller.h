/*
    Gamepad controller
*/

#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <SDL.h>
#include "shift_register.h"

// Controller status bits
const uint8_t BIT_Right = ( 1 << 7 );
const uint8_t BIT_Left = ( 1 << 6 );
const uint8_t BIT_Down = ( 1 << 5 );
const uint8_t BIT_Up = ( 1 << 4 );
const uint8_t BIT_Start = ( 1 << 3 );
const uint8_t BIT_Select = ( 1 << 2 );
const uint8_t BIT_B = ( 1 << 1 );
const uint8_t BIT_A = ( 1 << 0 );

class LatchValue
{
public:
    void set( uint8_t value )
    {
        m_value = value;
        m_bitsRemaining = 8;
    }

    uint8_t read()
    {
        // Emulate a standard NES controller
        if ( m_bitsRemaining == 0 )
            return ( 0x40 | 1 );

        const uint8_t value = ( m_value & 0x01 );
        m_value >>= 1;
        --m_bitsRemaining;
        return ( 0x40 | value );
    }

    uint8_t peek() const
    {
        return ( m_value & 0x01 );
    }

private:
    uint8_t m_value = 0;
    uint8_t m_bitsRemaining = 0;
};


class Controller
{
public:
    void Init();
    uint8_t ReadPlayer( int index );
    void WritePlayer( int index, uint8_t data );

private:
    void Update();

    struct Controller_t
    {
        SDL_GameController *controller = nullptr;
        bool strobing = true;
        LatchValue latch;
    } m_controller[2];
};
#endif // CONTROLLER_H