/*
    Memory address mappings
*/

#ifndef MEMORY_MAPS_H
#define MEMORY_MAPS_H


//
// CPU
//

const uint16_t MEM_ZeroPage = 0x0000;
const uint16_t MEM_Stack = 0x0100;
const uint16_t MEM_PRG = 0x8000;
const uint16_t MEM_PRG_Mirror = 0xC000; // Start address for mirrored PRG ROM

const uint16_t MEM_NMI_Vector = 0xFFFA;
const uint16_t MEM_Reset_Vector = 0xFFFC;

// NOTE: These are identical, just making them clear for the caller
const uint16_t MEM_IRQ_Vector = 0xFFFE;
const uint16_t MEM_BRK_Vector = 0xFFFE;


//
// PPU
//
const uint16_t MEM_PPU_PatternTables = 0x0000;

const uint16_t MEM_PPU_NameTables = 0x2000;
const uint16_t MEM_PPU_NameTable_Size = 0x0400;
const uint16_t MEM_PPU_NameTables_MirrorStart = 0x3000;
const uint16_t MEM_PPU_NameTables_MirrorEnd = 0x3EFF;

const uint16_t MEM_PPU_AttributeTable = 0x23C0;

const uint16_t MEM_PPU_Palette_Image = 0x3F00;
const uint16_t MEM_PPU_Palette_Sprites = 0x3F10;

const uint16_t MEM_PPU_Control = 0x2000;
const uint16_t MEM_PPU_Mask = 0x2001;
const uint16_t MEM_PPU_Status = 0x2002;
const uint16_t MEM_PPU_Scroll = 0x2005;
const uint16_t MEM_PPU_Address = 0x2006;
const uint16_t MEM_PPU_Data = 0x2007;
const uint16_t MEM_PPU_DirectMemoryAccess = 0x4014;

const uint16_t MEM_PPU_OAM_Address = 0x2003;
const uint16_t MEM_PPU_OAM_Data = 0x2004;
const uint16_t MEM_PPU_OAM_DMA = 0x4014;


//
// APU
//

const uint16_t MEM_APU_Pulse1 = 0x4000;
const uint16_t MEM_APU_Pulse2 = 0x4004;
const uint16_t MEM_APU_Triangle = 0x4008;
const uint16_t MEM_APU_Noise = 0x400C;
const uint16_t MEM_APU_DMC = 0x4010;
const uint16_t MEM_APU_Status = 0x4015;
const uint16_t MEM_APU_ChannelEnable = 0x4015;
const uint16_t MEM_APU_FrameCounter = 0x4017;


//
// Controller
//
const uint16_t MEM_Controller_One = 0x4016;
const uint16_t MEM_Controller_Two = 0x4017;

#endif // MEMORY_MAPS_H