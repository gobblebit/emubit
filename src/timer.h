#ifndef TIMER_H
#define TIMER_H

#include <chrono>
inline uint32_t GetTicks()
{
    auto now = std::chrono::high_resolution_clock::now();
    auto duration = now.time_since_epoch();
    return static_cast<uint32_t>( std::chrono::duration_cast<std::chrono::milliseconds>( duration ).count() );
}

class Timer
{
public:
    Timer()
    {
        m_startTime = SDL_GetTicks();
    }

    void Stop()
    {
        m_duration = (double)( SDL_GetTicks() - m_startTime );
    }

    double GetDuration() const
    {
        return m_duration;
    }

private:
    uint32_t m_startTime = 0;
    double m_duration = -1.0;
};

class AccumulatingTimer
{
public:
    AccumulatingTimer()
    {
        m_startTime = 0;
    }

    void Start()
    {
        m_startTime = SDL_GetTicks();
    }

    void Reset()
    {
        m_duration = 0.0;
        m_startTime = SDL_GetTicks();
    }

    void Accumulate()
    {
        m_duration += (double)( SDL_GetTicks() - m_startTime );
    }

    double GetDuration() const
    {
        return m_duration;
    }

private:
    uint32_t m_startTime = 0;
    double m_duration = 0.0;
};

#endif // TIMER_H