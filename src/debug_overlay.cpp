#include "base.h"

#include <vector>
#include <algorithm>

#include <SDL.h>

#include "debug_overlay.h"

DebugOverlay::~DebugOverlay()
{
    SDL_DestroyTexture( m_overlay );
}

//
//
//

void DebugOverlay::Init()
{
    m_overlay = SDL_CreateTexture( gRenderer, SDL_PIXELFORMAT_RGB888, SDL_TEXTUREACCESS_STREAMING, 256, 256 );
    SDL_SetTextureBlendMode( m_overlay, SDL_BLENDMODE_BLEND );

    // Get all captures ready to go
    for ( int i = 0; i < k_Captures_Size; ++i )
    {
        m_Captures[i].reset();
    }
}

//
//
//

void DebugOverlay::RenderGraph( const FrameCapture_t &newCapture )
{
    // Take the new capture onboard
    m_Captures[m_CurrentCapture] = newCapture;
    m_Captures[m_CurrentCapture].valid = true;
    m_CurrentCapture = ( m_CurrentCapture + 1 ) % ARRAYSIZE( m_Captures );

    double longestTime = 0.0;

    // Render our known captures
    const double frameTime = 16.66667;
    int captureID = m_CurrentCapture;
    for ( int i = 0; i < k_Captures_Size; ++i )
    {
        const FrameCapture_t &capture = m_Captures[i];
        if ( !capture.valid )
            continue;

        const double renderFramePerc = ( capture.renderTime / frameTime );
        const double CPUFramePerc = ( capture.CPUTime / frameTime );
        const double PPUFramePerc = ( capture.PPUTime / frameTime );
        const double APUFramePerc = ( capture.APUTime / frameTime );
        const double knownTime = ( capture.CPUTime + capture.PPUTime + capture.APUTime );
        ASSERT( knownTime <= capture.totalTime );
        const double unknownFramePerc = ( capture.totalTime - knownTime ) / frameTime;

        longestTime = max( longestTime, capture.totalTime );

        const int x = 150 + i;
        {
            const int y = 230 - (int)floor( ( capture.totalTime / frameTime ) * 20.0f );
            SDL_SetRenderDrawColor( gRenderer, 128, 128, 128, 255 );
            SDL_RenderDrawLine( gRenderer, x, y, x, 230 );
        }

        struct RenderGroup_t
        {
            double time;
            uint8_t r, g, b;
        };

        // Sort the times from slowest to fastest so we can always see them on top of
        // one another

        // clang-format off
        std::vector<RenderGroup_t> groups = {
            { renderFramePerc, 255, 0,   0 },
            { CPUFramePerc,    0,   255, 0 },
            { APUFramePerc,    0,   0,   255 },
            { unknownFramePerc,255, 255, 255 } };

        std::sort( groups.begin(), groups.end(), []( auto &lhs, auto &rhs ) {
            return ( lhs.time > rhs.time );
        } );
        // clang-format on

        for ( auto &group : groups )
        {
            const int y = 230 - (int)floor( group.time * 20.0f );
            SDL_SetRenderDrawColor( gRenderer, group.r, group.g, group.b, 255 );
            SDL_RenderDrawLine( gRenderer, x, y, x, 230 );
        }
    }

    //
    // Border
    //

    SDL_Rect rect;
    rect.x = 150;
    rect.y = 210;
    rect.w = 101;
    rect.h = 22;

    if ( longestTime > 16.666667 )
    {
        SDL_SetRenderDrawColor( gRenderer, 255, 0, 0, 255 );
    }
    else
    {
        SDL_SetRenderDrawColor( gRenderer, 255, 255, 255, 255 );
    }

    SDL_RenderDrawRect( gRenderer, &rect );
}
