/*
    NES Emulator
*/

#include "base.h"

#include <SDL.h>
#include "thirdparty/nes_ntsc/nes_ntsc.h"
#include "thirdparty/nes_snd_emu/Simple_Apu.h"
#include "thirdparty/nes_snd_emu/Sound_Queue.h"

#include "timer.h"
#include "cart.h"
#include "controller.h"
#include "cpu.h"
#include "ppu.h"
#include "debug_overlay.h"


//
// APU emulator
//

Blip_Buffer *gSoundBuffer = nullptr;
Nes_Apu *gNESAPU = nullptr;
Sound_Queue *gSoundQueue = nullptr;
bool gSoundEnabled = true;

extern int _ReadDMC( void *, cpu_addr_t addr );

int gCPUCyclesElapsedThisFrame = 0;

void _CPU_AddCycles( uint16_t numCycles )
{
    gCPUCyclesElapsedThisFrame += numCycles;
}

cpu_time_t _CPU_TimeElapsed()
{
    return gCPUCyclesElapsedThisFrame;
}

const uint16_t k_Audio_SampleRate = 44100;
const uint16_t k_Audio_SampleSize = 128;
blip_sample_t gSampleBuffer[k_Audio_SampleSize];


//
// Basic components
//

Logger *gLogger = nullptr;
Cart *gCart = nullptr;
CPU *gCPU = nullptr;
PPU *gPPU = nullptr;
Mapper *gMapper = nullptr;
Controller *gController = nullptr;
DebugOverlay *gDebugOverlay = nullptr;
SDL_Renderer *gRenderer = nullptr;

#ifdef _TESTING
#include "testing.h"
TestHarness *gTestHarness = nullptr;
#endif // _TESTING

bool gPause = false;
bool gShowDebugOverlay = false;
bool gShowCHR = false;
bool gUseNTSCFilter = false;

//
//
//

void _SignalIRQ()
{
    gCPU->SignalIRQ();
}


//
//
//

bool gExit = false;
void _SignalExit( const char *reason = nullptr )
{
    SDL_ShowSimpleMessageBox( 0, "Exit", reason, nullptr );
    gExit = true;
}


//
//
//

bool loadCart( const char *pFilename )
{
    // Init the cart
    delete gCart;
    gCart = new Cart();

    if ( !gCart->load( pFilename ) )
        return false;

    // Setup the mapper
    gMapper = gCart->getMapper();
    gMapper->LoadPRG( gCart->getPRG(), gCart->getPRGSize() );
    gMapper->LoadCHR( gCart->getCHR(), gCart->getCHRSize() );
    gMapper->SetMirrorMode( gCart->getMirrorMode() );

    if ( gCart->containsBattery() )
    {
        gMapper->LoadSRAM( gCart->getSaveFilename() );
    }

    // Choose the proper entry point for the test
    uint16_t overrideAddr = MEM_Reset_Vector;
#ifdef _TESTING
    overrideAddr = gTestHarness->GetEntryPoint();
#endif // _TESTING

    gCPU->Reset( overrideAddr );

    return true;
}


//
//
//

bool Init()
{
    if ( SDL_Init( SDL_INIT_VIDEO | SDL_INIT_EVENTS | SDL_INIT_AUDIO | SDL_INIT_TIMER | SDL_INIT_GAMECONTROLLER ) < 0 )
        return false;

    // Calculate the smallest scale we can fit within our window at
    SDL_DisplayMode displayMode;
    SDL_GetCurrentDisplayMode( 0, &displayMode );
    const uint8_t scaleFactor = static_cast<uint8_t>( floor( max( displayMode.w, displayMode.h ) / max( gNativeWidth, gNativeHeight ) ) / 2 );

    auto pMainWindow = SDL_CreateWindow( "EmuBit",
                                         SDL_WINDOWPOS_CENTERED,
                                         SDL_WINDOWPOS_CENTERED,
                                         ( gNativeWidth * scaleFactor ),
                                         ( gNativeHeight * scaleFactor ),
                                         SDL_WINDOW_SHOWN | SDL_WINDOW_BORDERLESS );

    if ( pMainWindow == nullptr )
    {
        SDL_Quit();
        return false;
    }

    gRenderer = SDL_CreateRenderer( pMainWindow, -1, SDL_RENDERER_ACCELERATED );
    if ( gRenderer == nullptr )
    {
        SDL_Quit();
        return false;
    }

    SDL_RenderSetLogicalSize( gRenderer, gNativeWidth, gNativeHeight );
    SDL_ShowCursor( false );

    //
    // Testing harness
    //

#ifdef _TESTING
    gTestHarness = new TestHarness();
    if ( !gTestHarness->Init() )
        return false;
#endif // _TESTING

    //
    // Create the core components
    //

    gLogger = new Logger();

    gCPU = new CPU();
    gPPU = new PPU();

    gNESAPU = new Nes_Apu();
    gSoundQueue = new Sound_Queue();

    //
    // Debug overlay
    //

    gDebugOverlay = new DebugOverlay();
    gDebugOverlay->Init();

    //
    // Controller
    //

    gController = new Controller();
    gController->Init();

    //
    // Load our cart
    //

    const char *cartName = nullptr;

    //
    // Tests
    //

    // cartName = "data/tests/cpu/nestest.NES";                     // (PASS)
    // cartName = "data/tests/cpu/instr_test-v5/official_only.NES"; // (PASS)

    // cartName = "data/tests/nestress.NES"; // (FAIL)

    // cartName = "data/tests/cpu/cpu_interrupts_v2/cpu_interrupts.NES"; // (FAILS)
    // cartName = "data/tests/cpu/cpu_exec_space/test_cpu_exec_space_ppuio.NES"; // (FAILS)
    // cartName = "data/tests/cpu/cpu_exec_space/test_cpu_exec_space_apu.NES";   // (FAILS)

    // cartName = "data/tests/sprite0/ppu_sprite_hit.NES";

    //
    // Games
    //

    //
    // MMC1
    //

    // cartName = "data/Blaster Master.NES";
    // cartName = "data/Castlevania 2.NES";
    // cartName = "data/Dr Mario.NES";
    // cartName = "data/Metroid.NES";
    // cartName = "data/Mega Man 2.NES";
    // cartName = "data/Final Fantasy.NES";
    // cartName = "data/Kid Icarus.NES";
    // cartName = "data/Bionic Commando.NES";
    // cartName = "data/Adventures of Bayou Billy.NES";
    // cartName = "data/Dragon Warrior.NES";
    cartName = "data/Ninja Gaiden.NES";
    // cartName = "data/Boy and His Blob.NES";
    // cartName = "data/RC Pro-Am.NES";
    // cartName = "data/Bases Loaded.NES";
    // cartName = "data/Golgo 13.NES";
    // cartName = "data/Tetris.NES";
    // cartName = "data/Blades of Steel.NES";
    // cartName = "data/Zelda 1.NES";
    // cartName = "data/Journey to Silius.NES";
    // cartName = "data/Qix.NES";
    // cartName = "data/Rad Racer.NES";
    // cartName = "data/Faxanadu.NES";
    // cartName = "data/Willow.NES";
    // cartName = "data/Majou Densetsu 2.NES";

    // cartName = "data/Teenage Mutant Ninja Turtles.NES"; // (FLICKERING SPRITES)
    // cartName = "data/Maniac Mansion.NES"; // (BAD SPRITE0 IN INTRO)
    // cartName = "data/Guardian Legend.NES"; // (STATUS BAR ISSUES)
    // cartName = "data/Double Dragon.NES"; // (BAD SPRITE0)
    // cartName = "data/Commando.NES"; // (BAD SPRITES/NT)
    // cartName = "data/California Games.NES"; // (SPRITE0 ISSUES)
    // cartName = "data/Bubble Bobble.NES"; // (INSANE BACKGROUND PALETTE)

    //
    // MMC2
    //

    // cartName = "data/Mike Tyson's Punch-Out!!.NES";

    //
    // MMC3
    //

    // cartName = "data/Double Dragon 2.NES";
    // cartName = "data/Ninja Gaiden 2.NES";
    // cartName = "data/River City Ransom.NES";
    // cartName = "data/Final Fantasy 3.NES";
    // cartName = "data/Karnov.NES";
    // cartName = "data/Kirby's Adventure.NES";
    // cartName = "data/Mega Man 3.NES";
    // cartName = "data/Mega Man 4.NES";
    // cartName = "data/Mega Man 5.NES";
    // cartName = "data/Metal Storm.NES";
    // cartName = "data/Smash TV.NES";
    // cartName = "data/Super Mario Bros 2.NES";
    // cartName = "data/Super Mario Bros 3.NES";
    // cartName = "data/Zelda 2.NES";
    // cartName = "data/Power Blade.NES";
    // cartName = "data/Super C.NES";
    // cartName = "data/RBI Baseball.NES";
    // cartName = "data/Batman.NES";

    // cartName = "data/Kabuki - Quantum Fighter.NES"; // (Hangs before main menu)

    //
    // MMC6
    //

    // cartName = "data/Startropics.NES";

    //
    // AOROM
    //

    // cartName = "data/Wizards & Warriors.NES";
    // cartName = "data/Battletoads.NES"; // (MASSIVE JITTER + CRASH)

    //
    // UNROM
    //

    // cartName = "data/Castlevania.NES";
    // cartName = "data/Contra.NES";
    // cartName = "data/Mega Man.NES";
    // cartName = "data/Rygar.NES";
    // cartName = "data/Ghosts'n Goblins.NES";
    // cartName = "data/Metal Gear.NES";
    // cartName = "data/Life Force.NES";
    // cartName = "data/Top Gun.NES";
    // cartName = "data/Final Fantasy 2.NES";
    // cartName = "data/Ikari Warriors.NES";
    // cartName = "data/Swords and Serpents.NES";
    // cartName = "data/Snake Rattle'n Roll.NES";
    // cartName = "data/Jackal.NES";
    // cartName = "data/Legendary Wings.NES";

    // cartName = "data/Rush'n Attack.NES"; // (SLIGHT SPRITE0 SPLIT)
    // cartName = "data/Section Z.NES"; // (SLIGHT SPRITE0 SPLIT)
    // cartName = "data/Micro Machines.NES"; // (BAD SPRITE0 SPLIT)
    // cartName = "data/Duck Tales.NES"; // (SLIGHT SPRITE0 TEARING)

    //
    // CNROM
    //

    cartName = "data/Goonies 2.NES";
    // cartName = "data/Donkey Kong.nes";
    // cartName = "data/Donkey Kong Classics.NES";
    // cartName = "data/Arkanoid.NES";
    // cartName = "data/Adventure Island Classic.NES";
    // cartName = "data/Goonies.NES";
    // cartName = "data/Solomon's Key.NES";
    // cartName = "data/Gradius.NES";
    // cartName = "data/Jaws.NES";
    // cartName = "data/Milon's Secret Castle.NES";
    // cartName = "data/Balloon Fight.NES";
    // cartName = "data/Excitebike.NES";
    // cartName = "data/Ice Climber.NES";
    // cartName = "data/Kung Fu.NES";
    // cartName = "data/Paperboy.NES";
    // cartName = "data/Gyromite.NES";
    // cartName = "data/Super Mario Bros.NES";
    // cartName = "data/Bump'n'Jump.NES";
    // cartName = "data/Friday the 13th.NES";

    //
    // GxROM
    //

    // cartName = "data/Gumshoe.NES";

#ifdef _TESTING
    // Overwrite anything that's come before this
    cartName = gTestHarness->GetROMFilename();
#endif // _TESTING

    if ( !loadCart( cartName ) )
    {
        SDL_ShowSimpleMessageBox( 0, "Cart Error", gCart->GetErrorMessage(), nullptr );
        return false;
    }

    if ( gSoundEnabled )
    {
        gSoundBuffer = new Blip_Buffer();
        gSoundBuffer->sample_rate( k_Audio_SampleRate );
        gSoundBuffer->clock_rate( 1789773 );

        gNESAPU->output( gSoundBuffer );
        gNESAPU->dmc_reader( _ReadDMC );

        gSoundQueue->init( k_Audio_SampleRate );
    }

    return true;
}


//
//
//

void Shutdown()
{
    // Save out our SRAM
    if ( gCart->containsBattery() )
    {
        gMapper->SaveSRAM( gCart->getSaveFilename() );
    }

    gMapper->Shutdown();

#ifdef _TESTING
    delete gTestHarness;
#endif // _TESTING

    delete gSoundBuffer;
    delete gNESAPU;
    delete gSoundQueue;

    delete gPPU;
    delete gCPU;
    delete gDebugOverlay;
    delete gController;
    delete gCart;
    delete gLogger;

    SDL_DestroyRenderer( gRenderer );

    SDL_Quit();
}


//
//
//

void UpdateSound()
{
    if ( !gSoundEnabled )
        return;

    // Update our emulation engine
    const int cyclesElapsed = _CPU_TimeElapsed();
    gNESAPU->end_frame( cyclesElapsed );
    gSoundBuffer->end_frame( cyclesElapsed );

    // Fill the sound queue
    auto samples_avail = gSoundBuffer->samples_avail();
    while ( samples_avail > 0 )
    {
        long sampleCount = gSoundBuffer->read_samples( gSampleBuffer, k_Audio_SampleSize );
        gSoundQueue->write( gSampleBuffer, sampleCount );
        samples_avail -= sampleCount;
    }

    gCPUCyclesElapsedThisFrame = 0;
}


//
//
//

void UpdateEvents()
{
    SDL_Event event;
    while ( SDL_PollEvent( &event ) )
    {
        switch ( event.type )
        {
            case SDL_KEYDOWN:
                if ( event.key.keysym.scancode == SDL_SCANCODE_ESCAPE )
                {
                    gPause = !gPause;
                }
                else if ( event.key.keysym.scancode == SDL_SCANCODE_D )
                {
                    gShowDebugOverlay = !gShowDebugOverlay;
                }
                else if ( event.key.keysym.scancode == SDL_SCANCODE_C )
                {
                    gShowCHR = !gShowCHR;
                }
                else if ( event.key.keysym.scancode == SDL_SCANCODE_F )
                {
                    gUseNTSCFilter = !gUseNTSCFilter;
                }
                else if ( event.key.keysym.scancode == SDL_SCANCODE_Q )
                {
                    gExit = true;
                }
                break;

            case SDL_QUIT:
                gExit = true;
                break;
        }
    }
}


//
//
//

void RunFrame()
{
    DebugOverlay::FrameCapture_t frameCapture;
    AccumulatingTimer frameTime;
    bool flipped = false;

    uint32_t lastFrametime = GetTicks();

    while ( !gExit )
    {
        if ( gPause )
        {
            // Wait for us to be released from the pause
            UpdateEvents();
            SDL_Delay( 100 );
            continue;
        }

        frameTime.Start();

        //
        // Deal with SDL events
        //

        // Update our time delta
        const uint32_t currentTime = GetTicks();
        const double deltaTime = ( currentTime - lastFrametime ) * 0.001;
        lastFrametime = currentTime;

        const uint32_t k_cylesPerFrame = static_cast<uint32_t>( 1023000.0 * deltaTime );

        int numCPUCycles = 0;

        while ( numCPUCycles < (int)k_cylesPerFrame )
        {
            //
            // CPU
            //

            Timer count;
            {
                numCPUCycles = gCPU->Run();
                _CPU_AddCycles( numCPUCycles );
            }
            count.Stop();
            frameCapture.CPUTime += count.GetDuration();


            //
            // PPU
            //

            {
                gPPU->Run( numCPUCycles * k_PPUCyclesPerCPU );
                frameCapture.PPUTime += gPPU->GetRenderDuration();
            }

            //
            // Test harness
            //

#ifdef _TESTING
            TestState_t state = gTestHarness->Update();
            switch ( state )
            {
                case TestState_t::Ready:
                case TestState_t::Running:
                    break;
                case TestState_t::Failed:
                {
                    char output[256];
                    snprintf( output, ARRAYSIZE( output ), "FAILED: %s\n\n%s\n", gTestHarness->GetROMFilename(), gTestHarness->GetErrorText() );
                    gLogger->LogString( output );
                    _SignalExit( output );
                    break;
                }
                case TestState_t::Succeeded:
                {
                    gLogger->Log( "PASSED: '%s'\n", gTestHarness->GetROMFilename() );
                    gTestHarness->ProgressToNextTest();
                    if ( gTestHarness->Completed() )
                    {
                        _SignalExit( "All tests passed!" );
                        continue;
                    }

                    // Load the next test cart
                    if ( !loadCart( gTestHarness->GetROMFilename() ) )
                    {
                        char errorText[128];
                        snprintf( errorText, sizeof( errorText ), "Cannot load test harness cart: %s\n", gTestHarness->GetROMFilename() );
                        _SignalExit( errorText );
                    }

                    // Skip any VBlank work
                    continue;
                }
            }
#endif // _TESTING

            //
            // VBlank period
            //

            if ( gPPU->inVBlank() )
            {
                // Wait for us to leave VBlank
                flipped = false;
                frameTime.Accumulate();
                continue;
            }

            //
            // Rendering
            //

            if ( flipped )
            {
                frameTime.Accumulate();
                continue;
            }

            //
            // Update sound
            //

            {
                // FIXME: Roll this into a singular class instead of doing it this way
                Timer count;

                UpdateSound();

                count.Stop();
                frameCapture.APUTime += count.GetDuration();
            }

            //
            // Present the screen
            //

            // NOTE: We're missing the full timing on the render capture here...
            frameTime.Accumulate();
            frameCapture.totalTime = frameTime.GetDuration();
            frameTime.Start();

            UpdateEvents();

            {
                Timer count;
                static const SDL_Rect rect = { 0, 0, gNativeWidth, gNativeHeight };

                SDL_SetRenderTarget( gRenderer, nullptr );
                SDL_Rect src;
                if ( gUseNTSCFilter )
                {
                    src.w = 602;
                    gPPU->RenderNTSCFilter();
                }
                else
                {
                    src.w = 256;
                }

                src.h = 240;
                src.x = src.y = 0;
                SDL_RenderCopy( gRenderer, gPPU->GetFramebuffer(), &src, &rect );

                if ( gShowCHR )
                {
                    SDL_RenderCopy( gRenderer, gPPU->GetCHRPage(), &rect, nullptr );
                }

                count.Stop();
                frameCapture.renderTime = count.GetDuration();

                if ( gShowDebugOverlay )
                {
                    gDebugOverlay->RenderGraph( frameCapture );
                    SDL_RenderCopy( gRenderer, gDebugOverlay->GetOverlay(), &rect, nullptr );
                }

                SDL_RenderPresent( gRenderer );
                flipped = true;
            }

            //
            // Keep our framerate within 1/60th of a second
            //

            frameTime.Accumulate();
        }

        //
        // Restart for the next frame
        //

        frameTime.Reset();
        frameCapture.reset();
    }
}


//
//
//

int APIENTRY WinMain( HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow )
{
    if ( !Init() )
        return 0;

    RunFrame();

    Shutdown();

    return 0;
}
