#ifndef LOGGER_H
#define LOGGER_H

#include <fstream>

class Logger
{
public:
    Logger()
    {
        for ( int i = 0; i < k_Log_NumEntries; ++i )
        {
            m_LogEntries[i][0] = 0;
        }
    }

    ~Logger()
    {
        if ( m_recordLog )
        {
            std::ofstream logFile;
            logFile.open( "log.txt" );
            int currentIndex = m_nextEntry;
            for ( int i = 0; i < k_Log_NumEntries; ++i, ++currentIndex )
            {
                currentIndex = currentIndex % k_Log_NumEntries;
                if ( m_LogEntries[currentIndex][0] == 0 )
                    continue;

                logFile << m_LogEntries[currentIndex];
            }
            logFile.close();
        }
    }

    bool IsLogging() const
    {
        return m_recordLog;
    }

    void LogString( const char *string )
    {
        if ( !m_recordLog )
            return;

        strncpy_s( m_LogEntries[m_nextEntry], string, sizeof( LogEntry_t ) );

        m_nextEntry = ( m_nextEntry + 1 ) % k_Log_NumEntries;
    }

    void Log( const char *format, ... )
    {
        if ( !m_recordLog )
            return;

        va_list args;
        va_start( args, format );
        vsnprintf( m_LogEntries[m_nextEntry], sizeof( LogEntry_t ), format, args );
        va_end( args );

        m_nextEntry = ( m_nextEntry + 1 ) % k_Log_NumEntries;
    }

private:
    bool m_recordLog = false;

    // Cycling ring of strings that contains the last lines of executed code
    static const int k_Log_NumEntries = ( 1024 * 128 );
    int m_nextEntry = 0;
    typedef char LogEntry_t[256];
    LogEntry_t m_LogEntries[k_Log_NumEntries];
};

#endif // LOGGER_H