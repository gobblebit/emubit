#ifndef MAPPER_H
#define MAPPER_H

#include "base.h"

#include <map>

class Mapper
{
    //
    // Mirror mode
    //
public:
    void SetMirrorMode( MirrorMode mode )
    {
        m_mirrorMode = mode;
    }

    virtual MirrorMode GetMirrorMode() const
    {
        return m_mirrorMode;
    }

    uint16_t TranslateAddress( uint16_t addr ) const
    {
        // Handle for nametable mirroring
        if ( addr >= MEM_PPU_NameTables && addr < MEM_PPU_NameTables_MirrorStart )
        {
            const uint8_t nametableIndex = ( addr & ~0x2000 ) >> 10;
            const uint16_t tAddr = addr & ~( 1 << 11 ); // Always turn off the upper bit
            switch ( GetMirrorMode() )
            {
                case MirrorMode::Horizontal:
                    return ( tAddr & ~( 1 << 10 ) ) | ( k_Mirror_Horizontal[nametableIndex] << 10 );
                case MirrorMode::Vertical:
                    return ( tAddr & ~( 1 << 10 ) ) | ( k_Mirror_Vertical[nametableIndex] << 10 );
                case MirrorMode::Single_A:
                    return ( tAddr & ~( 1 << 10 ) ) | ( k_Mirror_SingleA[nametableIndex] << 10 );
                case MirrorMode::Single_B:
                    return ( tAddr & ~( 1 << 10 ) ) | ( k_Mirror_SingleB[nametableIndex] << 10 );
                default:
                    return addr;
            }
        }

        return addr;
    }

private:
    MirrorMode m_mirrorMode = MirrorMode::Horizontal;

    inline static const uint8_t k_Mirror_Horizontal[] = { 0, 0, 1, 1 };
    inline static const uint8_t k_Mirror_Vertical[] = { 0, 1, 0, 1 };
    inline static const uint8_t k_Mirror_SingleA[] = { 0, 0, 0, 0 };
    inline static const uint8_t k_Mirror_SingleB[] = { 1, 1, 1, 1 };

    //
    // PRG
    //

public:
    void LoadPRG( const uint8_t *data, size_t size )
    {
        delete[] m_PRG;
        m_PRGSize = size;
        m_PRG = new uint8_t[m_PRGSize];
        memcpy( m_PRG, data, size );
        OnPRGLoaded();
    }

    void LoadCHR( const uint8_t *data, size_t size )
    {
        delete[] m_CHR;

        if ( size > 0 )
        {
            m_CHRSize = size;
            m_CHR = new uint8_t[m_CHRSize];
            memcpy( m_CHR, data, size );
        }
        else
        {
            // Give us RAM to play with
            m_CHRSize = 0x2000;
            m_CHR = new uint8_t[m_CHRSize];
        }

        OnCHRLoaded();
    }

    virtual void SaveSRAM( const char *filename )
    {
        //
    }

    virtual void LoadSRAM( const char *filename )
    {
        //
    }

protected:
    enum class Serialize
    {
        ToDisk,
        FromDisk
    };

    void SerializeSRAM( uint8_t *SRAM, size_t size, const char *filename, Serialize type )
    {
        switch ( type )
        {
            case Serialize::ToDisk:
            {
                std::ofstream file( filename, std::ios::out | std::ios::binary );
                if ( !file.is_open() )
                {
                    // TODO: Complain!
                    return;
                }

                file.write( (char *)SRAM, size );
                break;
            }
            case Serialize::FromDisk:
            {
                std::ifstream file( filename, std::ios::in | std::ios::binary );
                if ( !file.is_open() )
                {
                    // TODO: Complain!
                    return;
                }

                file.read( (char *)SRAM, size );
                break;
            }
        }
    }

protected:
    uint8_t *m_PRG = nullptr;
    size_t m_PRGSize = 0;

    uint8_t *m_CHR = nullptr;
    size_t m_CHRSize = 0;

public:
    virtual void Init(){};
    virtual void Shutdown()
    {
        delete[] m_PRG;
        m_PRG = nullptr;
        m_PRGSize = 0;

        delete[] m_CHR;
        m_CHR = nullptr;
        m_CHRSize = 0;
    }

    virtual void OnPRGLoaded(){};
    virtual void OnCHRLoaded(){};

    virtual void SignalA12Changed( bool newState ){};

    virtual void PRG_WriteByte( uint16_t addr, uint8_t data ) = 0;
    virtual uint8_t PRG_ReadByte( uint16_t addr ) = 0;

    virtual void CHR_WriteByte( uint16_t addr, uint8_t data ) = 0;
    virtual uint8_t CHR_ReadByte( uint16_t addr ) = 0;
};

// FIXME: Might not be the best layout...

#include "mapper_nrom.h"
#include "mapper_cnrom.h"
#include "mapper_axrom.h"
#include "mapper_gxrom.h"
#include "mapper_unrom.h"
#include "mapper_87.h"
#include "mapper_mmc1.h"
#include "mapper_mmc2.h"
#include "mapper_mmc3.h"

static const std::map<uint8_t, Mapper *> gMappers = {
    // clang-format off
    { 0, new Mapper_NROM() }, 
    { 1, new Mapper_MMC1() }, 
    { 2, new Mapper_UNROM() }, 
    { 3, new Mapper_CNROM() }, 
    { 4, new Mapper_MMC3() },
    { 7, new Mapper_AxROM() }, 
    { 9, new Mapper_MMC2() }, 
    { 66, new Mapper_GxROM() },
    { 87, new Mapper_87() },
    // clang-format on
};

#endif // MAPPER_H