#ifndef MAPPER_MMC1_H
#define MAPPER_MMC1_H

#include <array>
#include "shift_register.h"

class Mapper_MMC1 : public Mapper
{
public:
    Mapper_MMC1() : m_SRAM()
    {
        memset( m_SRAM, 0, ARRAYSIZE( m_SRAM ) );
    }

    virtual void Init() override
    {
        m_Registers.control.mirroring = 2;
        m_Registers.control.fixLast = 1;
        m_Registers.control.swap16k = 1;
        m_Registers.CHRBank0 = 0;
        m_Registers.CHRBank1 = 1;
        m_Registers.PRGBank = 0;
    }

    virtual MirrorMode GetMirrorMode() const
    {
        switch ( m_Registers.control.mirroring )
        {
            case 0:
                return MirrorMode::Single_A;
            case 1:
                return MirrorMode::Single_B;
            case 2:
                return MirrorMode::Vertical;
            case 3:
            default:
                return MirrorMode::Horizontal;
        }
    }

    const uint16_t k_PRG_BankSize = 0x4000;

    virtual void OnPRGLoaded() override
    {
        m_numBanks = static_cast<uint16_t>( m_PRGSize / k_PRG_BankSize );
    }

    const uint16_t k_CHR_BankSize = 0x1000;

    virtual void OnCHRLoaded() override
    {
        m_numCHRBanks = static_cast<uint16_t>( m_CHRSize / k_CHR_BankSize );
    }

private:
    uint16_t m_numBanks = 0;
    uint16_t m_numCHRBanks = 0;

public:
    virtual void PRG_WriteByte( uint16_t addr, uint8_t data ) override
    {
        // SRAM
        if ( addr < 0x8000 )
        {
            m_SRAM[addr - 0x6000] = data;
            return;
        }

        // Shift register clear
        if ( data & ( 1 << 7 ) )
        {
            m_shiftRegister.reset();
            return;
        }

        m_shiftRegister.addBit( data & ( 1 << 0 ) );

        const uint8_t signalBit = 3;
        if ( m_shiftRegister.isFull( signalBit ) )
        {
            uint8_t value = m_shiftRegister.get( signalBit );
            m_shiftRegister.reset();

            // Control
            if ( addr < 0xA000 )
            {
                m_Registers.control.value = ( value & 0x1F );
                return;
            }

            // CHR Bank 0
            if ( addr < 0xC000 )
            {
                m_Registers.CHRBank0 = ( value & 0x1F );

                if ( GetCHRBankMode() == CHRBankMode::Switch8k )
                {
                    // Ignore the lowest bit
                    m_Registers.CHRBank0 >>= 1;
                }

                return;
            }

            // CHR Bank 1
            if ( addr < 0xE000 )
            {
                m_Registers.CHRBank1 = ( value & 0x1F );
                return;
            }

            // PRG Bank
            m_Registers.PRGBank = ( value & 0x0F );
            m_Registers.PRGChipEnabled = ( ( value & 0x10 ) == 0 );

            if ( GetPRGBankSize() == 0x8000 )
            {
                // Ignore the lowest bit
                m_Registers.PRGBank >>= 1;
            }
        }
    }

    virtual uint8_t PRG_ReadByte( uint16_t addr ) override
    {
        // SRAM
        if ( addr < 0x8000 )
            return m_SRAM[addr - 0x6000];

        // Full bank swap
        const uint16_t bankSize = GetPRGBankSize();
        if ( bankSize == 0x8000 )
        {
            const uint32_t tAddr = ( addr - bankSize ) + ( m_Registers.PRGBank * bankSize );
            ASSERT( tAddr < m_PRGSize );
            return m_PRG[tAddr];
        }

        switch ( GetPRGBankMode() )
        {
            case PRGBankMode::SwapFirstFixLast:
            {
                // Switch first, fix last
                if ( addr < 0xC000 )
                    return m_PRG[( addr & ~0x8000 ) + ( m_Registers.PRGBank * bankSize )];

                return m_PRG[( addr - 0xC000 ) + ( ( m_numBanks - 1 ) * bankSize )];
            }
            case PRGBankMode::FixFirstSwapLast:
            {
                // Switch last, fix first
                if ( addr < 0xC000 )
                    return m_PRG[addr & ~0x8000];

                return m_PRG[( addr - 0xC000 ) + ( m_Registers.PRGBank * bankSize )];
            }
            default:
                ASSERT( 0 );
                break;
        }

        // FIXME: We failed to find a valid mapping!
        ASSERT( 0 );
        return 0x00;
    }

    virtual void CHR_WriteByte( uint16_t addr, uint8_t data ) override
    {
        // In 8k mode we ignore any banking
        if ( GetCHRBankMode() == CHRBankMode::Switch8k )
        {
            m_CHR[( m_Registers.CHRBank0 * 0x2000 ) + addr] = data;
            return;
        }

        // Otherwise allow the bank switch
        const uint16_t bank = ( addr < 0x1000 ) ? m_Registers.CHRBank0 : m_Registers.CHRBank1;
        m_CHR[( addr % 0x1000 ) + ( 0x1000 * bank )] = data;
    }

    virtual uint8_t CHR_ReadByte( uint16_t addr ) override
    {
        // In 8k mode we ignore any banking
        if ( GetCHRBankMode() == CHRBankMode::Switch8k )
            return m_CHR[( m_Registers.CHRBank0 * 0x2000 ) + addr];

        // Otherwise allow the bank switch
        const uint16_t bank = ( addr < 0x1000 ) ? m_Registers.CHRBank0 : m_Registers.CHRBank1;
        return m_CHR[( addr % 0x1000 ) + ( bank * 0x1000 )];
    }

private:
    struct Control_t
    {
        union
        {
            uint8_t value = 0;
            struct
            {
                uint8_t mirroring : 2;
                uint8_t fixLast : 1;
                uint8_t swap16k : 1;
                uint8_t CHRBankMode : 1;
            };
        };
    };

    ShiftRegister<uint8_t> m_shiftRegister;
    struct Registers
    {
        Control_t control = {};
        uint8_t CHRBank0 = 0;
        uint8_t CHRBank1 = 1;
        uint8_t PRGBank = 0;
        bool PRGChipEnabled = false;
    } m_Registers;


    enum class PRGBankMode
    {
        SwapFirstFixLast,
        FixFirstSwapLast,
    };

    inline PRGBankMode GetPRGBankMode() const
    {
        return ( m_Registers.control.fixLast ) ? PRGBankMode::SwapFirstFixLast : PRGBankMode::FixFirstSwapLast;
    }

    inline uint16_t GetPRGBankSize() const
    {
        return ( m_Registers.control.swap16k ) ? 0x4000 : 0x8000;
    }

    enum class CHRBankMode
    {
        Switch4k,
        Switch8k,
    };

    inline CHRBankMode GetCHRBankMode() const
    {
        return ( m_Registers.control.CHRBankMode ) ? CHRBankMode::Switch4k : CHRBankMode::Switch8k;
    }

    //
    // SRAM
    //

private:
    uint8_t m_SRAM[0x2000]; // $6000-$8000

public:
    virtual void SaveSRAM( const char *filename ) override
    {
        SerializeSRAM( m_SRAM, ARRAYSIZE( m_SRAM ), filename, Serialize::ToDisk );
    }

    virtual void LoadSRAM( const char *filename ) override
    {
        SerializeSRAM( m_SRAM, ARRAYSIZE( m_SRAM ), filename, Serialize::FromDisk );
    }
};

#endif // MAPPER_MMC1_H