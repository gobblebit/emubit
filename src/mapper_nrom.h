#ifndef MAPPER_NROM_H
#define MAPPER_NROM_H

#include <vector>

class Mapper_NROM : public Mapper
{
public:
    virtual void Init() override
    {
        memset( m_SRAM, 0, ARRAYSIZE( m_SRAM ) );
        Shutdown();
    }

    virtual void PRG_WriteByte( uint16_t addr, uint8_t data ) override
    {
        ASSERT( addr >= 0x6000 );

        // SRAM
        if ( addr < 0x8000 )
        {
            m_SRAM[addr - 0x6000] = data;
            return;
        }

        m_PRG[addr & ~0x8000] = data;
    }

    virtual uint8_t PRG_ReadByte( uint16_t addr ) override
    {
        ASSERT( addr >= 0x6000 );

        // SRAM
        if ( addr < 0x8000 )
            return m_SRAM[addr - 0x6000];

        if ( m_PRGSize <= ( 16 * 1024 ) )
            return m_PRG[addr % ( 16 * 1024 )];

        return m_PRG[addr & ~0x8000];
    }

    virtual void CHR_WriteByte( uint16_t addr, uint8_t data ) override
    {
        ASSERT( addr < 0x2000 );
        m_CHR[addr] = data;
    }

    virtual uint8_t CHR_ReadByte( uint16_t addr ) override
    {
        ASSERT( addr < 0x2000 );
        return m_CHR[addr];
    }

private:
    uint8_t m_SRAM[0x2000]; // $6000-$8000

public:
    virtual void SaveSRAM( const char *filename ) override
    {
        SerializeSRAM( m_SRAM, ARRAYSIZE( m_SRAM ), filename, Serialize::ToDisk );
    }

    virtual void LoadSRAM( const char *filename ) override
    {
        SerializeSRAM( m_SRAM, ARRAYSIZE( m_SRAM ), filename, Serialize::FromDisk );
    }
};

#endif // MAPPER_NROM_H