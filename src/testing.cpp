/*
    Testing suite for compatibility
*/

#include "base.h"

#include "cpu.h"

#include "testing.h"

//
// Fill out all the test units for running
//

bool TestHarness::Init()
{
    //
    // CPU basic
    //
    // clang-format off
    m_Tests.push_back( { 
        "data/tests/nestest.NES", 
        0xC000,
        [this]() { 
            const uint8_t testNumber = gCPU->ReadByte( 0x0001 );
            const uint8_t testResult = gCPU->ReadByte( 0x0002 );
            const uint8_t testResultEx = gCPU->ReadByte( 0x0003 ); // NOTE: Currently unused

            // Something went wrong
            if ( testResult != 0x00 )
            {
                snprintf( m_errorText, ARRAYSIZE(m_errorText), "%#04X", testResult );
                return TestState_t::Failed;
            }

            // Current test running
            if ( testNumber != 0xFF )
                return TestState_t::Running;

            return TestState_t::Succeeded;
        }
    } );
    // clang-format on

    //
    // CPU extensive
    //

    std::function<TestState_t( void )> fnBlarggType = [this]() {
        const uint8_t testRunning[3] = { gCPU->ReadByte( 0x6001 ), gCPU->ReadByte( 0x6002 ), gCPU->ReadByte( 0x6003 ) };

        // Wait for the signal that we're ready to assess the state
        if ( testRunning[0] != 0xDE || testRunning[1] != 0xB0 || testRunning[2] != 0x61 )
            return TestState_t::Running;

        const uint8_t testResult = gCPU->ReadByte( 0x6000 );
        if ( testResult == 0x80 )
            return TestState_t::Running;

        // NOTE: This means the test wants a reset in at least 100ms
        if ( testResult == 0x81 )
        {
            ASSERT( 0 );
            return TestState_t::Failed;
        }

        if ( testResult == 0x00 )
            return TestState_t::Succeeded;

        // Take the error text
        uint16_t offset = 0;
        uint8_t errorChar = 0;
        do
        {
            errorChar = gCPU->ReadByte( 0x6004 + offset );
            m_errorText[offset] = errorChar;
            ++offset;
        } while ( errorChar != 0 );

        return TestState_t::Failed;
    };

    m_Tests.push_back( { "data/tests/cpu/instr_test-v5/official_only.NES", MEM_Reset_Vector, fnBlarggType } );
    // m_Tests.push_back( { "data/tests/sprite0/07.screen_bottom.NES", MEM_Reset_Vector, fnBlarggType } );
    // m_Tests.push_back( { "data/tests/ppu_vbl_nmi.NES", MEM_Reset_Vector, fnBlarggType } );
    m_Tests.push_back( { "data/tests/sprite0/ppu_sprite_hit.NES", MEM_Reset_Vector, fnBlarggType } );

    //
    // NROM
    //

    /*
    {
        // clang-format off
        m_Tests.push_back( {
            "data/tests/mapper/M0_P32K_C8K_V.NES",
            MEM_Reset_Vector,
            [this]() {
                //
                return TestState_t::Running;
            }
        } );
        // clang-format on
    }
    */

    return true;
}


//
// Update our current status
//
TestState_t TestHarness::Update()
{
    // FIXME: We should really complain to the user at this point
    if ( Completed() )
        return TestState_t::Ready;

    Test_t &test = m_Tests[m_currentTest];
    return test.testFn();
}
