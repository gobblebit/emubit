#ifndef MAPPER_MMC2_H
#define MAPPER_MMC2_H

class Mapper_MMC2 : public Mapper
{
public:
    Mapper_MMC2()
    {
        //
    }

    virtual void OnPRGLoaded()
    {
        m_PRG_Offset[0] = 0;

        // Setup our fixed last three banks
        const uint16_t numBanks = static_cast<uint16_t>( m_PRGSize >> 13 );
        for ( int i = 0; i < 3; ++i )
        {
            const uint16_t bankNum = ( numBanks - 1 ) - ( 2 - i );
            m_PRG_Offset[i + 1] = bankNum << 13;
        }
    }

    virtual void OnCHRLoaded()
    {
        // Layout the initial four banks
        const uint16_t numBanks = static_cast<uint16_t>( m_CHRSize >> 12 );
        uint8_t i = 0;
        for ( int bank = 0; bank < 2; ++bank )
        {
            for ( int latch = 0; latch < 2; ++latch )
            {
                m_CHR_Offset[bank][latch] = ( ( i++ ) << 12 );
            }
        }
    }

    virtual MirrorMode GetMirrorMode() const override
    {
        return m_MirrorMode;
    }

    virtual void PRG_WriteByte( uint16_t addr, uint8_t data ) override
    {
        const uint16_t bucket = addr >> 12;
        switch ( bucket )
        {
            case 0x000A:
                m_PRG_Offset[0] = ( data & 0x0F ) << 13;
                break;
            case 0x000B:
                m_CHR_Offset[0][0] = ( data & 0x1F ) << 12;
                break;
            case 0x000C:
                m_CHR_Offset[0][1] = ( data & 0x1F ) << 12;
                break;
            case 0x000D:
                m_CHR_Offset[1][0] = ( data & 0x1F ) << 12;
                break;
            case 0x000E:
                m_CHR_Offset[1][1] = ( data & 0x1F ) << 12;
                break;
            case 0x000F:
                m_MirrorMode = ( data & 0x01 ) ? MirrorMode::Horizontal : MirrorMode::Vertical;
                break;
            default:
                return;
        }
    }

    virtual uint8_t PRG_ReadByte( uint16_t addr ) override
    {
        /*
        const uint16_t bank = ( addr & ~0x8000 ) >> 13;
        const uint16_t tAddr = addr % 0x2000;
        const uint32_t offset = tAddr + m_PRG_Offset[bank];
        ASSERT( offset < m_PRGSize );
        return m_PRG[offset];
        */
        return m_PRG[( addr % 0x2000 ) + m_PRG_Offset[( addr & ~0x8000 ) >> 13]];
    }

    virtual void CHR_WriteByte( uint16_t addr, uint8_t data ) override
    {
        ASSERT( 0 );
    }

    virtual uint8_t CHR_ReadByte( uint16_t addr ) override
    {
        uint8_t oldLatch[2] = { m_Latch[0], m_Latch[1] };

        if ( addr == 0x0FD8 )
        {
            m_Latch[0] = 0;
        }
        else if ( addr == 0x0FE8 )
        {
            m_Latch[0] = 1;
        }
        else if ( addr >= 0x1FD8 && addr <= 0x1FDF )
        {
            m_Latch[1] = 0;
        }
        else if ( addr >= 0x1FE8 && addr <= 0x1FEF )
        {
            m_Latch[1] = 1;
        }

        /*
        const uint16_t bank = addr >> 12;
        const uint16_t tAddr = addr % 0x1000;
        const uint32_t offset = tAddr + m_CHR_Offset[bank][oldLatch[bank]];
        ASSERT( offset < m_CHRSize );
        return m_CHR[offset];
        */

        const uint16_t bank = addr >> 12;
        return m_CHR[( addr % 0x1000 ) + m_CHR_Offset[bank][oldLatch[bank]]];
    }

private:
    MirrorMode m_MirrorMode = MirrorMode::Horizontal;

    uint32_t m_PRG_Offset[4];
    uint32_t m_CHR_Offset[2][2];
    uint8_t m_Latch[2] = { 0, 0 };
};

#endif // MAPPER_MMC2_H