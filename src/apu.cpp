#include "base.h"

#include <ctime>
#include <algorithm>

#include <SDL.h>
#include "thirdparty/nes_snd_emu/Simple_Apu.h"

#include "apu.h"

extern Blip_Buffer *gSoundBuffer;

void audioCallback( void *userData, Uint8 *stream, int len )
{
    return;

    // Call on to the callback
    // ( (APU *)userData )->AudioCallback( stream, len );

    int16_t *int16_stream = (int16_t *)stream;
    int numSamples = len / 2;

    if ( gSoundBuffer->samples_avail() >= numSamples )
    {
        gSoundBuffer->read_samples( int16_stream, numSamples );
    }
    else
    {
        OutputDebugString( "Missed buffer fill!\n" );
    }
}


void APU::GenerateSamples( float *stream, int len, double frequency, WaveType waveform )
{
    const int numSamples = ( len / 4 ); // Bytes per 32-bit sample
    const double samplesPerSecond = 44100;
    const double sinePhaseInc = (double)frequency / samplesPerSecond;
    const double halfCycle = frequency / 2;
    static uint64_t nSample = 0;
    const int samplesPerPeriod = (int)floor( samplesPerSecond / frequency );

    for ( int i = 0; i < numSamples; ++i, /*m_sample += sinePhaseInc,*/ ++nSample )
    {
        float value = 0.0f;
        if ( frequency <= 8 )
        {
            stream[i] = 0.0f;
            continue;
        }

        if ( samplesPerPeriod > 0 )
        {
            value = (float)( (int)nSample % samplesPerPeriod ) / (float)samplesPerPeriod;
        }

        switch ( waveform )
        {
            case WaveType::Sine:
                ASSERT( 0 );
                // stream[i] = (float)( sin( 2.0 * M_PI * m_sample ) );
                break;
            case WaveType::Triangle:
            {
                // TODO: This should be quantized to a 4-bit value to emulate the hardware
                const float saw = ( 2.0f * value ) - 1.0f;
                stream[i] = ( 2.0f * (float)fabs( saw ) ) - 1.0f;
                break;
            }
            case WaveType::Saw:
                stream[i] = ( 2.0f * value ) - 1.0f;
                break;
            case WaveType::Square:
                stream[i] = ( value > 0.5f ) ? 1.0f : -1.0f;
                break;
            case WaveType::Noise:
                // FIXME: This doesn't approximate how the NES noise works!
                stream[i] = ( 2.0f * ( (float)std::rand() / (float)RAND_MAX ) ) - 1.0f;
                break;
        }
    }
}

void APU::AudioCallback( Uint8 *stream, int len )
{
    float *sampleWrite = (float *)stream;

#if false
    // FIXME: Silence for now
    for ( int i = 0; i < ( len / 4 ); ++i )
    {
        sampleWrite[i] = 0.0f;
    }

    return;
#endif // 0

    const int numSamples = ( len / 4 ); // Bytes per 32-bit sample

    if ( m_ChannelStates.triangle )
    {
        UpdateTriangle();
    }
    else
    {
        memset( m_Samples_Triangle, 0, sizeof( float ) * ARRAYSIZE( m_Samples_Triangle ) );
    }

    WaveType waveType = WaveType::Triangle;

    for ( int i = 0; i < numSamples; ++i )
    {
        /*
        // Input is 0..15 for all but DMC which is 0..127
        float pulseOut = ( m_Samples_Pulse1[i] + m_Samples_Pulse2[i] );
        float tndOut = ( m_Samples_Triangle[i] / 8227 ) + ( m_Samples_Noise[i] / 12241 ) + ( m_Samples_DMC[i] / 22638 ) + 100;
        if ( pulseOut > 0.0f )
        {
            pulseOut = 95.88f / ( ( 8128 / pulseOut ) + 100 );
        }

        if ( tndOut > 0.0f )
        {
            tndOut = 159.79f / ( ( 1 / tndOut ) + 100 );
        }

        sampleWrite[i] = pulseOut + tndOut;
        */
        sampleWrite[i] = m_Samples_Triangle[i];
    }
}


//
//
//

bool APU::Init()
{
    return true;

    // Init our sample buffers for each channel
    memset( m_Samples_Pulse1, 0, sizeof( float ) * ARRAYSIZE( m_Samples_Pulse1 ) );
    memset( m_Samples_Pulse2, 0, sizeof( float ) * ARRAYSIZE( m_Samples_Pulse2 ) );
    memset( m_Samples_Triangle, 0, sizeof( float ) * ARRAYSIZE( m_Samples_Triangle ) );
    memset( m_Samples_Noise, 0, sizeof( float ) * ARRAYSIZE( m_Samples_Noise ) );
    memset( m_Samples_DMC, 0, sizeof( float ) * ARRAYSIZE( m_Samples_DMC ) );

    // Init our SDL controller
    SDL_AudioSpec want, have;
    SDL_zero( want );
    want.freq = 44100;
    want.format = AUDIO_S16;
    want.channels = 1;
    want.samples = k_Num_Samples;
    want.callback = audioCallback;
    want.userdata = this;

    m_deviceID = SDL_OpenAudioDevice( NULL, 0, &want, &have, 0 /*SDL_AUDIO_ALLOW_FORMAT_CHANGE*/ );
    if ( m_deviceID == 0 )
    {
        gLogger->Log( "Failed to open audio: %s", SDL_GetError() );
        return false;
    }

    // Pause playback until we're actually ready to work
    Pause();

    // Seed our timer for the noise channel
    std::srand( (int)std::time( nullptr ) );
    return true;
}


//
//
//

void APU::Pause( bool pause /*=true*/ )
{
    SDL_PauseAudioDevice( m_deviceID, ( pause ) ? 1 : 0 );
}


//
//
//

void APU::Shutdown()
{
    Pause();
    SDL_Delay( 100 );
    SDL_CloseAudioDevice( m_deviceID );
}


//
//
//

void APU::UpdatePulse( const Pulse_t &pulse )
{
    //
}


//
//
//

void APU::UpdateTriangle()
{
    // const uint32_t timer = m_triangle.timer();
    const uint32_t timer = 314;
    const uint32_t frequency = ( k_CPU_Rate / ( 32 * ( timer + 1 ) ) ) / 8;

    // Generate triangle samples for the frame time
    GenerateSamples( m_Samples_Triangle, ARRAYSIZE( m_Samples_Triangle ), frequency, WaveType::Triangle );
}

void APU::Run( uint16_t numCycles )
{
    // APU cycles at half the CPU rate. Since we can be left with a remainder, we
    // pick it back up on the next run.
    const uint16_t apuCycles = ( numCycles / 2 ) + m_cycleRemainder;
    m_cycleRemainder = numCycles % 2; // Odd cycles carry over
}
