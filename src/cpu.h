#ifndef CPU_H
#define CPU_H

/* Emulation of a 6502 CPU */

#include <map>
#include <functional>
#include <vector>

#include "memory_maps.h"
#include "mapper.h"

const uint8_t BIT_Carry = 0;
const uint8_t BIT_Zero = 1;
const uint8_t BIT_Interrupt = 2;
const uint8_t BIT_Decimal = 3;
const uint8_t BIT_Break = 4;
const uint8_t BIT_AlwaysOne = 5;
const uint8_t BIT_Overflow = 6;
const uint8_t BIT_Negative = 7;

class CPU
{
public:
    CPU();

    void Reset( const uint16_t overrideAddr = MEM_Reset_Vector );
    uint8_t Run();

    void AddCycles( uint64_t numCycles )
    {
        m_Cycles += numCycles;
    }

    uint64_t GetNumCyclesElapsed() const
    {
        return m_Cycles;
    }

private:
    void RandomizeMemory();

    uint64_t m_Cycles = k_CPU_StartOffset; // Number of cycles we've currently executed (accounting for start-up cost)
    bool m_CrossedPage = false;            // Set if a memory operation crossed a page boundary (used for cycle counting)
    uint8_t m_Memory[64 * 1024];           // 64KB (primary bank)

    //
    // Registers
    //

    struct Registers
    {
        void init()
        {
            A = X = Y = 0;
            S = 0xFF;
            P = 0b00100100;
            PC = 0;
        };

        uint8_t A;   // Accumulator
        uint8_t X;   // X
        uint8_t Y;   // Y
        uint8_t S;   // Stack pointer
        uint8_t P;   // Status
        uint16_t PC; // Program counter
    } m_Registers;

    //
    // Opcodes
    //

    typedef std::function<int16_t()> Opcodes_t;
    Opcodes_t m_Opcodes[256];

    void PopulateOpcodes();

    //
    // Memory accessors
    //

    inline uint16_t TranslateAddress( uint16_t address ) const
    {
        // Lower mirrors
        if ( address <= 0x1FFFu )
            return address % 0x0800u;

        // PPU register mirrors
        if ( address >= 0x2000u && address <= 0x3FFFu )
            return 0x2000 + ( address % 0x08u );

        return address;
    }

public:
    inline uint8_t ReadByte( uint16_t addr ) const;

    inline uint16_t ReadWord( uint16_t addr ) const
    {
        return static_cast<uint16_t>( ( ReadByte( addr + 1 ) << 8 ) | ReadByte( addr ) );
    }

private:
    inline void WriteByte( uint16_t addr, uint8_t data );

    inline void WriteWord( uint16_t addr, uint16_t data )
    {
        WriteByte( addr, static_cast<uint8_t>( data & 0xFFu ) );
        WriteByte( addr + 1, static_cast<uint8_t>( data >> 8 ) );
    }

    friend class PPU;

    //
    // Helpers
    //

    inline void AddPageBoundaryPenalty()
    {
        if ( m_CrossedPage )
        {
            ++m_Cycles;
        }
    }

    inline uint16_t MAKE_WORD( const uint8_t *stack ) const
    {
        return static_cast<uint16_t>( ( stack[1] << 8 ) | stack[0] );
    }

    inline int16_t ExecuteBranch()
    {
        const int16_t offset = relative();
        const uint16_t preJump = m_Registers.PC + 1;
        const uint16_t postJump = preJump + offset;
        if ( ( preJump & 0xFF00 ) != ( postJump & 0xFF00 ) )
        {
            // Page boundary penalty
            ++m_Cycles;
        }

        return offset + 1;
    }

    //
    // Status register helpers
    //

    inline bool status_HasCarry() const
    {
        return m_Registers.P & ( 1 << BIT_Carry );
    }

    inline void status_SetCarry( bool state )
    {
        SET_BIT( m_Registers.P, BIT_Carry, state );
    }

    inline bool status_HasNegative() const
    {
        return m_Registers.P & ( 1 << BIT_Negative );
    }

    inline void status_SetNegative( uint16_t operand )
    {
        SET_BIT( m_Registers.P, BIT_Negative, ( operand & 0x80U ) );
    }

    inline bool status_HasZero() const
    {
        return m_Registers.P & ( 1 << BIT_Zero );
    }

    inline void status_SetZero( uint16_t operand )
    {
        SET_BIT( m_Registers.P, BIT_Zero, ( operand == 0x00U ) );
    }

    inline bool status_HasOverflow() const
    {
        return m_Registers.P & ( 1 << BIT_Overflow );
    }

    inline void status_SetOverflow( int16_t a, int16_t b, int16_t result )
    {
        const bool overflowed = !!( ( a ^ result ) & ( b ^ result ) & 0x80U );
        SET_BIT( m_Registers.P, BIT_Overflow, overflowed );
    }

    inline bool status_HasDecimal() const
    {
        return m_Registers.P & ( 1 << BIT_Decimal );
    }

    inline void status_SetDecimal( bool state )
    {
        SET_BIT( m_Registers.P, BIT_Decimal, state );
    }

    //
    // Value look-up helpers
    //

    inline int8_t relative() const
    {
        return static_cast<int8_t>( ReadByte( m_Registers.PC ) );
    }

    inline uint16_t absolute()
    {
        const uint16_t offset = ReadWord( m_Registers.PC );
        m_CrossedPage = ( offset > 0x00FFu );
        return offset;
    }

    inline uint16_t absoluteX()
    {
        uint16_t offset = ReadWord( m_Registers.PC );
        const uint8_t upper = ( offset >> 8 );
        offset += m_Registers.X;
        const uint8_t after = ( offset >> 8 );
        m_CrossedPage = ( after != upper );
        return offset;
    }

    inline uint16_t absoluteY()
    {
        uint16_t offset = ReadWord( m_Registers.PC );
        const uint8_t upper = ( offset >> 8 );
        offset += m_Registers.Y;
        const uint8_t after = ( offset >> 8 );
        m_CrossedPage = ( after != upper );
        return offset;
    }

    inline uint8_t zeroPage()
    {
        return ReadByte( m_Registers.PC );
    }

    inline uint8_t zeroPageX()
    {
        return ReadByte( m_Registers.PC ) + m_Registers.X;
    }

    inline uint8_t zeroPageY()
    {
        return ReadByte( m_Registers.PC ) + m_Registers.Y;
    }

    inline uint16_t indirectX()
    {
        const uint8_t pagedAddr = ReadByte( m_Registers.PC ) + m_Registers.X;
        const uint8_t pagedAddrNext = pagedAddr + (uint8_t)1;
        m_CrossedPage = ( pagedAddrNext < pagedAddr ); // FIXME: Used?
        const uint8_t lsb = ReadByte( pagedAddr );
        const uint8_t msb = ReadByte( pagedAddrNext );
        const uint16_t result = ( ( msb << 8 ) | lsb );
        return result;
    }

    inline uint16_t indirectY()
    {
        const uint8_t offset = ReadByte( m_Registers.PC );
        const uint8_t nextOffset = static_cast<uint8_t>( offset + 1 );
        const uint8_t lsb = ReadByte( offset );
        const uint8_t msb = ReadByte( nextOffset );
        const uint16_t addr = ( msb << 8 ) | lsb;
        const uint16_t finalAddr = static_cast<uint16_t>( addr + m_Registers.Y );
        m_CrossedPage = ( finalAddr < addr ) | ( nextOffset < offset );
        return finalAddr;
    }

    inline uint16_t indirect()
    {
        // FIXME: Adopt a non-branching version!
        const uint8_t highBit = ReadByte( m_Registers.PC + 1 );
        const uint8_t lowBit = ReadByte( m_Registers.PC );
        m_CrossedPage = ( lowBit == 0xFF );
        if ( m_CrossedPage )
        {
            // LSB from 0x??FF
            const uint8_t lsb = ReadByte( static_cast<uint16_t>( ( highBit << 8 ) | lowBit ) ) & 0xFF;
            // MSB from 0x??00
            const uint8_t msb = ReadByte( static_cast<uint16_t>( ( highBit << 8 ) | 0x00u ) ) & 0xFF;
            return ( msb << 8 ) | lsb;
        }

        return ReadWord( ReadWord( m_Registers.PC ) );
    }

    inline uint8_t immediate()
    {
        return ReadByte( m_Registers.PC );
    }

    //
    // Instructions
    //

private:
    inline void LDA( uint8_t data )
    {
        m_Registers.A = data;
        status_SetZero( static_cast<uint16_t>( data ) );
        status_SetNegative( static_cast<uint16_t>( data ) );
    }

    inline void LDX( uint8_t data )
    {
        m_Registers.X = data;
        status_SetZero( static_cast<uint16_t>( data ) );
        status_SetNegative( static_cast<uint16_t>( data ) );
    }

    inline void LDY( uint8_t data )
    {
        m_Registers.Y = data;
        status_SetZero( static_cast<uint16_t>( data ) );
        status_SetNegative( static_cast<uint16_t>( data ) );
    }

    inline void STA( uint16_t addr )
    {
        WriteByte( addr, m_Registers.A );
    }

    inline void STX( uint16_t addr )
    {
        WriteByte( addr, m_Registers.X );
    }

    inline void STY( uint16_t addr )
    {
        WriteByte( addr, m_Registers.Y );
    }

    inline void ADC( uint8_t data )
    {
        const int16_t carry = ( status_HasCarry() ) ? 1 : 0;
        const uint16_t sum = m_Registers.A + data + carry;
        const uint8_t result = sum & 0xFFU;
        status_SetCarry( sum >> 8 );
        status_SetOverflow( m_Registers.A, data, result );
        status_SetZero( result );
        status_SetNegative( result );
        m_Registers.A = result;
    }

    inline void SBC( uint8_t data )
    {
        ADC( data ^ 0xFFU );
    }

    inline void DEC( uint16_t addr )
    {
        const uint8_t data = ReadByte( addr ) - 1;
        WriteByte( addr, data );
        status_SetZero( data );
        status_SetNegative( data );
    }

    inline void INC( uint16_t addr )
    {
        const uint8_t data = ReadByte( addr ) + 1;
        WriteByte( addr, data );
        status_SetZero( data );
        status_SetNegative( data );
    }

    inline void AND( uint8_t data )
    {
        m_Registers.A &= data;
        status_SetNegative( m_Registers.A );
        status_SetZero( m_Registers.A );
    }

    inline uint8_t _ASL( uint8_t data )
    {
        uint16_t fullResult = data * 2;
        status_SetCarry( fullResult >> 8 );
        uint8_t result = ( fullResult & 0xFFu );
        SET_BIT( result, 0, false );
        status_SetZero( result );
        status_SetNegative( result );
        return result;
    }

    inline void ASL( uint16_t addr )
    {
        WriteByte( addr, _ASL( ReadByte( addr ) ) );
    }

    inline void BIT( uint8_t data )
    {
        const uint8_t result = m_Registers.A & data;
        status_SetZero( result );
        SET_BIT( m_Registers.P, BIT_Overflow, data & ( 1 << 6 ) );
        SET_BIT( m_Registers.P, BIT_Negative, data & ( 1 << 7 ) );
    }

    inline void EOR( uint8_t data )
    {
        m_Registers.A ^= data;
        status_SetZero( m_Registers.A );
        status_SetNegative( m_Registers.A );
    }

    inline uint8_t _LSR( uint8_t data )
    {
        status_SetCarry( data & ( 1 << BIT_Carry ) );
        uint8_t result = data >> 1;
        SET_BIT( result, 7, false );
        status_SetZero( result );
        status_SetNegative( result );
        return result;
    }

    inline void LSR( uint16_t addr )
    {
        WriteByte( addr, _LSR( ReadByte( addr ) ) );
    }

    inline void ORA( uint8_t data )
    {
        m_Registers.A |= data;
        status_SetNegative( m_Registers.A );
        status_SetZero( m_Registers.A );
    }

    inline uint8_t _ROL( uint8_t data )
    {
        const bool hadCarry = status_HasCarry();
        status_SetCarry( data & ( 1 << 7 ) );
        uint8_t result = data << 1;
        SET_BIT( result, 0, hadCarry );
        status_SetZero( result );
        status_SetNegative( result );
        return result;
    }

    inline void ROL( uint16_t addr )
    {
        WriteByte( addr, _ROL( ReadByte( addr ) ) );
    }

    inline uint8_t _ROR( uint8_t data )
    {
        const bool hadCarry = status_HasCarry();
        status_SetCarry( data & ( 1 << 0 ) );
        uint8_t result = data >> 1;
        SET_BIT( result, 7, hadCarry );
        status_SetZero( result );
        status_SetNegative( result );
        return result;
    }

    inline void ROR( uint16_t addr )
    {
        WriteByte( addr, _ROR( ReadByte( addr ) ) );
    }

    inline void CMP( uint8_t data )
    {
        status_SetCarry( m_Registers.A >= data );
        SET_BIT( m_Registers.P, BIT_Zero, m_Registers.A == data );
        status_SetNegative( static_cast<uint16_t>( m_Registers.A - data ) );
    }

    inline void CPX( uint8_t data )
    {
        status_SetCarry( m_Registers.X >= data );
        SET_BIT( m_Registers.P, BIT_Zero, m_Registers.X == data );
        status_SetNegative( static_cast<uint16_t>( m_Registers.X - data ) );
    }

    inline void CPY( uint8_t data )
    {
        status_SetCarry( m_Registers.Y >= data );
        SET_BIT( m_Registers.P, BIT_Zero, m_Registers.Y == data );
        status_SetNegative( static_cast<uint16_t>( m_Registers.Y - data ) );
    }

    //
    // NMI
    //

private:
    void SignalNMIPending()
    {
        // NOTE: We need to wait until the next instruction to execute this
        m_NMIPending = 2;
    }

    void ExecuteNMI();

private:
    int m_NMIPending = 0;

    //
    // IRQ
    //

public:
    void SignalIRQ()
    {
        m_IRQPending = 1;
    }

private:
    void ExecuteIRQ();

private:
    int m_IRQPending = 0;

    //
    // Logging
    //

private:
    void LogCycle();
};

#endif // CPU_H