#ifndef MAPPER_AXROM_H
#define MAPPER_AXROM_H

class Mapper_AxROM : public Mapper_NROM
{
public:
    virtual uint8_t PRG_ReadByte( uint16_t addr ) override
    {
        return m_PRG[( m_Register.PRGBank << 15 ) + ( addr & ~0x8000 )];
    }

    virtual void PRG_WriteByte( uint16_t addr, uint8_t data ) override
    {
        if ( addr >= 0x8000 )
        {
            m_Register.value = data;
        }
    }

    virtual MirrorMode GetMirrorMode() const
    {
        return ( m_Register.nametable ) ? MirrorMode::Single_B : MirrorMode::Single_A;
    }

private:
    struct
    {
        union
        {
            uint8_t value;
            struct
            {
                uint8_t PRGBank : 3;
                uint8_t : 1;
                uint8_t nametable : 1;
            };
        };
    } m_Register;
};

#endif // MAPPER_AXROM_H