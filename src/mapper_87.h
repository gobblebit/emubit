#ifndef MAPPER_87_H
#define MAPPER_87_H

class Mapper_87 : public Mapper_CNROM
{
public:
    virtual void PRG_WriteByte( uint16_t addr, uint8_t data ) override
    {
        if ( addr >= 0x6000 && addr < 0x8000 )
        {
            m_bank = ( ( data & 0x01 ) << 1 ) | ( ( data & 0x02 ) >> 1 );
        }
    }
};

#endif // MAPPER_CNROM_H