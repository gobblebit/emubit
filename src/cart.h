/*
    Cart wrapper for iNES formatted ROMS
*/

#ifndef CART_H
#define CART_H

#include "mapper.h"

class Cart
{
public:
    Cart()
    {
        memset( &m_Options, 0, sizeof( m_Options ) );
        m_ErrorMessage[0] = 0;
    }

    ~Cart()
    {
        m_PRG.clear();
        m_PRG.resize( 0 );

        m_CHR.clear();
        m_CHR.resize( 0 );
    }

    Mapper *getMapper() const
    {
        return m_Mapper;
    }

private:
    uint8_t m_Trainer[512]; // FIXME: This is just garbage to us at the momenet

    Mapper *m_Mapper = nullptr;

    std::vector<uint8_t> m_PRG;
    std::vector<uint8_t> m_CHR;

    struct Options
    {
        // 0
        uint8_t Mirroring : 1; // 0: horizontal (vertical arrangement) (CIRAM A10 = PPU A11)
                               // 1: vertical (horizontal arrangement) (CIRAM A10 = PPU A10)
        uint8_t ContainsBattery : 1;
        uint8_t TrainerBeforePRG : 1;    // 512-byte trainer at $7000-$71FF (stored before PRG data)
        uint8_t IgnoreMirrorControl : 1; // Ignore mirroring control or above mirroring bit; instead provide four-screen VRAM
        uint8_t LowerMapper : 4;

        // 1
        uint8_t VSUnisystem : 1;
        uint8_t PlayChoice_10 : 1; // (8KB of Hint Screen data stored after CHR data)
        uint8_t NES2Format : 2;    // If equal to 2, flags 8-15 are in NES 2.0 format
        uint8_t UpperMapper : 4;
    } m_Options;

    struct Options_iNES
    {
        // 0
        uint8_t PRGSize : 8; // Rarely used

        // 1
        uint8_t IsPAL : 1; // Otherwise NTSC
        uint8_t : 7;       // Reserved

        // 2
        uint8_t TVSystem : 2;   // (0: NTSC; 2: PAL; 1/3: dual compatible)
        uint8_t : 2;            // Unused
        uint8_t RPGPresent : 1; // PRG RAM ($6000-$7FFF) (0: present; 1: not present)
        uint8_t HasBusConflicts : 1;
    } m_Options_iNES;

    struct Options_iNES2
    {
        // 0
        uint8_t HighestMapper : 4; // Highest nybble of mapper ID
        uint8_t Submapper : 4;

        // 1
        uint8_t PRGSize_MSB : 4;
        uint8_t CHRSize_MSB : 4;

        // 2
        uint8_t PRG_RAM_Size : 4;
        uint8_t PRG_NVRAM_Size : 4;

        // 3
        uint8_t CHR_RAM_Size : 4;
        uint8_t CHR_NVRAM_Size : 4;

        // 4
        uint8_t TimingMode : 2;
        uint8_t : 6; // UNUSED

        // 5
        uint8_t PPUType : 4;
        uint8_t HardwareType : 4;

        // 6
        uint8_t MiscRoms : 2;
        uint8_t : 6;

        // 7
        uint8_t DefaultExpansionDevice : 6;
        uint8_t : 2;
    } m_Options_iNES2;

    bool m_isINES2Format = false;

public:
    const std::vector<uint8_t> &getPRGROM() const
    {
        return m_PRG;
    }

    const size_t getPRGSize() const
    {
        return m_PRG.size();
    }

    const uint8_t *getPRG( uint32_t offset = 0 ) const
    {
        return m_PRG.data() + offset;
    }

    const size_t getCHRSize() const
    {
        return m_CHR.size();
    }

    const uint8_t *getCHR( uint32_t offset = 0 ) const
    {
        return m_CHR.data() + offset;
    }

    uint8_t getMapperID() const
    {
        // return ( m_Options.UpperMapper << 4 ) | m_Options.LowerMapper;
        return m_Options.LowerMapper;
    }

    MirrorMode getMirrorMode() const
    {
        return ( m_Options.Mirroring ) ? MirrorMode::Vertical : MirrorMode::Horizontal;
    }

    bool containsBattery() const
    {
        return m_Options.ContainsBattery;
    }

    bool load( const char *pFilename )
    {
        std::ifstream file( pFilename, std::ios::in | std::ios::binary );
        if ( !file.is_open() )
        {
            snprintf( m_ErrorMessage, ARRAYSIZE( m_ErrorMessage ), "Unable to locate cart: %s\n", pFilename );
            return false;
        }

        // Store our filename for SRAM serialization (minus extension)
        // FIXME: This isn't going to stand up to much trickery...
        for ( int i = 0; i < ARRAYSIZE( m_saveFilename ); ++i )
        {
            if ( pFilename[i] == '.' || pFilename[i] == '\0' )
            {
                m_saveFilename[i] = '\0';
                break;
            }

            m_saveFilename[i] = pFilename[i];
        }

        strncat_s( m_saveFilename, k_SaveFile_Extension, strlen( k_SaveFile_Extension ) );

        char header[4];
        if ( file.read( header, sizeof( header ) ) )
        {
            // Verify the header matches
            if ( header[0] != 'N' && header[1] != 'E' && header[2] != 'S' && header[3] != '\0' )
            {
                snprintf( m_ErrorMessage, ARRAYSIZE( m_ErrorMessage ), "Invalid iNES Header!\n" );
                file.close();
                return false;
            }
        }

        // PRG ROM size multiple
        char PRGSizeMultiple = 0;
        file.read( &PRGSizeMultiple, sizeof( PRGSizeMultiple ) );
        uint32_t PRGSize = PRGSizeMultiple * ( 16 * 1024 );

        // CHR ROM size multiple
        char CHRSizeMultiple = 0;
        file.read( &CHRSizeMultiple, sizeof( CHRSizeMultiple ) );
        uint32_t CHRSize = CHRSizeMultiple * ( 8 * 1024 );

        // Options (shared)
        file.read( reinterpret_cast<char *>( &m_Options ), sizeof( m_Options ) );
        if ( m_Options.NES2Format != 2 )
        {
            // iNES Format
            file.read( reinterpret_cast<char *>( &m_Options_iNES ), sizeof( m_Options_iNES ) );
            m_isINES2Format = false;
        }
        else
        {
            // iNES 2.0 Format
            file.read( reinterpret_cast<char *>( &m_Options_iNES2 ), sizeof( m_Options_iNES2 ) );
            m_isINES2Format = true;
        }

        // Verify we have a valid mapper
        auto mapper = gMappers.find( getMapperID() );
        if ( mapper == gMappers.end() )
        {
            snprintf( m_ErrorMessage, ARRAYSIZE( m_ErrorMessage ), "Invalid Mapper: %d\n", getMapperID() );
            file.close();
            return false;
        }

        m_Mapper = mapper->second;
        m_Mapper->Init();

        // Just fast-forward past the header section
        file.seekg( 16 );

        // Grab the trainer (if any)
        if ( m_Options.TrainerBeforePRG )
        {
            ASSERT( 0 );
            file.read( reinterpret_cast<char *>( &m_Trainer ), sizeof( m_Trainer ) );
            // TODO: Copy this data to 0x7000 in the CPU!
        }

        // Calculate iNES2.0 format sizes
        if ( m_isINES2Format )
        {
            PRGSize = ( m_Options_iNES2.PRGSize_MSB << 8 ) | ( PRGSizeMultiple );
            if ( m_Options_iNES2.PRGSize_MSB < 0xF )
            {
                PRGSize *= ( 16 * 1024 );
            }
            else
            {
                // TODO: Solve crazy notation
            }
        }

        // Grab the PRG memory
        m_PRG.resize( PRGSize );
        if ( !file.read( reinterpret_cast<char *>( m_PRG.data() ), PRGSize ) )
        {
            ASSERT( 0 );
        }

        // Grab any CHR memory
        if ( CHRSize > 0 )
        {
            // Calculate iNES2.0 format sizes
            if ( m_isINES2Format )
            {
                CHRSize = ( m_Options_iNES2.CHRSize_MSB << 8 ) | ( CHRSizeMultiple );
                if ( m_Options_iNES2.CHRSize_MSB < 0xF )
                {
                    CHRSize *= ( 8 * 1024 );
                }
                else
                {
                    // TODO: Solve crazy notation
                }
            }

            m_CHR.resize( CHRSize );
            if ( !file.read( reinterpret_cast<char *>( m_CHR.data() ), CHRSize ) )
            {
                ASSERT( 0 );
            }
        }

        // TODO: Support extra data on the image
        ASSERT( !m_isINES2Format || ( m_isINES2Format && m_Options_iNES2.MiscRoms == 0 ) );

        file.close();
        return true;
    }

    //
    // Error message
    //

public:
    const char *GetErrorMessage() const
    {
        return m_ErrorMessage;
    };

private:
    char m_ErrorMessage[512];

    //
    // Save files
    //

public:
    inline static const char *k_SaveFile_Extension = ".jwsave";

    const char *getSaveFilename()
    {
        return m_saveFilename;
    }

private:
    char m_saveFilename[512];
};


#endif // CART_H