#ifndef MAPPER_GXROM_H
#define MAPPER_GXROM_H

class Mapper_GxROM : public Mapper
{
public:
    virtual void PRG_WriteByte( uint16_t addr, uint8_t data ) override
    {
        if ( addr < 0x8000 )
            return;

        m_PRG_Offset = ( data & 0x03 ) << 0x0F;
        m_CHR_Offset = ( ( data >> 4 ) & 0x03 ) << 0x0D;
    }

    virtual uint8_t PRG_ReadByte( uint16_t addr ) override
    {
        return m_PRG[( addr & ~0x8000 ) + m_PRG_Offset];
    }

    virtual void CHR_WriteByte( uint16_t addr, uint8_t data ) override
    {
        ASSERT( 0 );
    }

    virtual uint8_t CHR_ReadByte( uint16_t addr ) override
    {
        return m_CHR[( addr % ~0x2000 ) + m_CHR_Offset];
    }

private:
    uint32_t m_PRG_Offset = 0x00000000;
    uint32_t m_CHR_Offset = 0x00000000;
};

#endif // MAPPER_GXROM_H