/*
    Controller
*/

#include "base.h"

#include <SDL.h>

#include "controller.h"

//
//
//

void Controller::Init()
{
    for ( int i = 0; i < SDL_NumJoysticks(); ++i )
    {
        if ( SDL_IsGameController( i ) )
        {
            if ( m_controller[0].controller == nullptr )
            {
                m_controller[0].controller = SDL_GameControllerOpen( i );
                continue;
            }

            m_controller[1].controller = SDL_GameControllerOpen( i );
            break;
        }
    }
}


//
//
//

void Controller::Update()
{
    for ( int i = 0; i < 2; ++i )
    {
        SDL_GameController *controller = m_controller[i].controller;
        uint8_t flags = 0;

        //
        // Gamepad
        //
        if ( controller )
        {
            if ( SDL_GameControllerGetButton( controller, SDL_CONTROLLER_BUTTON_DPAD_UP ) )
            {
                flags |= BIT_Up;
            }
            else if ( SDL_GameControllerGetButton( controller, SDL_CONTROLLER_BUTTON_DPAD_DOWN ) )
            {
                flags |= BIT_Down;
            }

            if ( SDL_GameControllerGetButton( controller, SDL_CONTROLLER_BUTTON_DPAD_LEFT ) )
            {
                flags |= BIT_Left;
            }
            else if ( SDL_GameControllerGetButton( controller, SDL_CONTROLLER_BUTTON_DPAD_RIGHT ) )
            {
                flags |= BIT_Right;
            }

            if ( SDL_GameControllerGetButton( controller, SDL_CONTROLLER_BUTTON_BACK ) )
            {
                flags |= BIT_Select;
            }

            if ( SDL_GameControllerGetButton( controller, SDL_CONTROLLER_BUTTON_START ) )
            {
                flags |= BIT_Start;
            }

            // FIXME: These are reversed with my crappy USB controller!
            if ( SDL_GameControllerGetButton( controller, SDL_CONTROLLER_BUTTON_A ) )
            {
                flags |= BIT_B;
            }

            if ( SDL_GameControllerGetButton( controller, SDL_CONTROLLER_BUTTON_B ) )
            {
                flags |= BIT_A;
            }
        }

        // The keyboard may only act as player one
        if ( i == 1 )
        {
            m_controller[i].latch.set( flags );
            break;
        }

        //
        // Keyboard
        //
        const uint8_t *state = SDL_GetKeyboardState( nullptr );
        if ( state[SDL_SCANCODE_UP] )
        {
            flags |= BIT_Up;
        }
        else if ( state[SDL_SCANCODE_DOWN] )
        {
            flags |= BIT_Down;
        }

        if ( state[SDL_SCANCODE_LEFT] )
        {
            flags |= BIT_Left;
        }
        else if ( state[SDL_SCANCODE_RIGHT] )
        {
            flags |= BIT_Right;
        }

        if ( state[SDL_SCANCODE_MINUS] )
        {
            flags |= BIT_Select;
        }

        if ( state[SDL_SCANCODE_EQUALS] )
        {
            flags |= BIT_Start;
        }

        if ( state[SDL_SCANCODE_S] )
        {
            flags |= BIT_A;
        }

        if ( state[SDL_SCANCODE_A] )
        {
            flags |= BIT_B;
        }

        m_controller[i].latch.set( flags );
    }
}


//
//
//

void Controller::WritePlayer( int index, uint8_t data )
{
    Controller_t &controller = m_controller[index];
    if ( data & 0x01 )
    {
        controller.strobing = true;
        Update();
    }
    else
    {
        controller.strobing = false;
    }
}

uint8_t Controller::ReadPlayer( int index )
{
    Controller_t &controller = m_controller[index];
    if ( controller.strobing )
        return controller.latch.peek();

    return controller.latch.read();
}
