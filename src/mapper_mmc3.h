#ifndef MAPPER_MMC3_H
#define MAPPER_MMC3_H

extern void _SignalIRQ();
extern uint16_t _PPU_GetCycle();
extern uint16_t _PPU_GetScanline();
extern void _PPU_A12_Changed( bool newState );
extern void _Log( const char *format, ... );

class Mapper_MMC3 : public Mapper
{
public:
    Mapper_MMC3() : m_SRAM()
    {
        memset( m_SRAM, 0, ARRAYSIZE( m_SRAM ) );
    }

    virtual void SignalA12Changed( bool newState )
    {
        if ( newState )
        {
            // Force a delay before we can respond to the change in A12
            const uint16_t thisCycle = _PPU_GetCycle();
            const uint16_t thisScanline = _PPU_GetScanline();
            if ( thisScanline == m_IRQ.lastChangeCycle )
                return;

            m_IRQ.lastChangeCycle = thisScanline;

            // Transfer the latch
            if ( m_IRQ.reload || m_IRQ.value == 0 )
            {
                m_IRQ.value = m_IRQ.latch;
                m_IRQ.reload = false;
                return;
            }

            // Decrement
            --m_IRQ.value;

            // NOTE: We only trigger this on the decrement that causes zero, not afterwards!
            if ( m_IRQ.value == 0 )
            {
                if ( m_IRQ.enabled )
                {
                    _SignalIRQ();
                }
            }
        }
    }

    virtual MirrorMode GetMirrorMode() const
    {
        if ( m_Mirror_Horizontal )
            return MirrorMode::Horizontal;

        return MirrorMode::Vertical;
    }

private:
    bool m_Mirror_Horizontal = true;

    const uint16_t k_PRG_BankSize = 0x2000;

public:
    virtual void OnPRGLoaded() override
    {
        m_numPRGBanks = static_cast<uint16_t>( m_PRGSize / k_PRG_BankSize );

        m_PRG_BankOffsets_Fixed[1] = static_cast<uint32_t>( m_PRGSize - k_PRG_BankSize );
        m_PRG_BankOffsets_Fixed[0] = m_PRG_BankOffsets_Fixed[1] - k_PRG_BankSize;
    }

private:
    uint16_t m_numPRGBanks = 0;

    //
    // PRG Banks
    //

private:
    uint32_t m_PRG_BankOffsets_Switchable[2] = { 0x0000, 0x2000 };
    uint32_t m_PRG_BankOffsets_Fixed[2] = { 0x4000, 0x6000 };
    bool m_PRG_WriteProtected = false;
    bool m_PRG_RAM_Enabled = false;

    //
    // CHR Banks
    //

public:
    virtual void OnCHRLoaded() override
    {
        m_numCHRBanks = static_cast<uint16_t>( m_CHRSize / k_CHR_BankSize );
    }

private:
    const uint16_t k_CHR_BankSize = 0x0400;
    uint16_t m_numCHRBanks = 0;

private:
    uint32_t m_CHR_BankOffsets[6] = { 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000 };

    // 7  bit  0
    // ---- ----
    // CPMx xRRR
    // |||   |||
    // |||   +++- Specify which bank register to update on next write to Bank Data register
    // |||        0: Select 2 KB CHR bank at PPU $0000-$07FF (or $1000-$17FF);
    // |||        1: Select 2 KB CHR bank at PPU $0800-$0FFF (or $1800-$1FFF);
    // |||        2: Select 1 KB CHR bank at PPU $1000-$13FF (or $0000-$03FF);
    // |||        3: Select 1 KB CHR bank at PPU $1400-$17FF (or $0400-$07FF);
    // |||        4: Select 1 KB CHR bank at PPU $1800-$1BFF (or $0800-$0BFF);
    // |||        5: Select 1 KB CHR bank at PPU $1C00-$1FFF (or $0C00-$0FFF);
    // |||        6: Select 8 KB PRG ROM bank at $8000-$9FFF (or $C000-$DFFF);
    // |||        7: Select 8 KB PRG ROM bank at $A000-$BFFF
    // ||+------- Nothing on the MMC3, see MMC6
    // |+-------- PRG ROM bank mode (0: $8000-$9FFF swappable,
    // |                                $C000-$DFFF fixed to second-last bank;
    // |                             1: $C000-$DFFF swappable,
    // |                                $8000-$9FFF fixed to second-last bank)
    // +--------- CHR A12 inversion (0: two 2 KB banks at $0000-$0FFF,
    //                                  four 1 KB banks at $1000-$1FFF;
    //                               1: two 2 KB banks at $1000-$1FFF,
    //                                  four 1 KB banks at $0000-$0FFF)

    struct BankSelectRegister_t
    {
        union
        {
            uint8_t value;
            struct
            {
                uint8_t targetBank : 3;
                uint8_t : 3; // Unused
                uint8_t PRGMode : 1;
                uint8_t CHRMode : 1;
            };
        };
    } m_bankRegister;


    //
    // IRQ
    //

private:
    struct IRQ_t
    {
        uint8_t latch = 0;            // Value that WILL be loaded at the appropriate time
        uint8_t value = 0;            // Current value we're decrementing towards the IRQ event
        bool reload = false;          // A reload is requested
        bool enabled = false;         // IRQ is active
        uint16_t lastChangeCycle = 0; // The last cycle this went from low to high
    } m_IRQ;

    //
    // PRG
    //

public:
    virtual void PRG_WriteByte( uint16_t addr, uint8_t data ) override
    {
        // SRAM
        if ( addr < 0x8000 )
        {
            m_SRAM[addr - 0x6000] = data;
            return;
        }

        const bool isEven = ( ( addr % 2 ) == 0 );

        // Bank selections
        if ( addr < 0xA000 )
        {
            // Bank select
            if ( isEven )
            {
                m_bankRegister.value = data;
                return;
            }

            // TODO: Collapse into 8 registers, not separate ones?

            // Bank data
            if ( m_bankRegister.targetBank <= 5 )
            {
                const uint8_t bank = data % m_numCHRBanks;
                m_CHR_BankOffsets[m_bankRegister.targetBank] = bank << 10;
            }
            else
            {
                const int index = m_bankRegister.targetBank - 6;
                const uint8_t bank = ( data & 0x3F ) % m_numPRGBanks;
                m_PRG_BankOffsets_Switchable[index] = bank << 13;
            }

            return;
        }

        //
        // Mirror / PRG RAM protect
        //

        if ( addr < 0xC000 )
        {
            if ( isEven )
            {
                m_Mirror_Horizontal = ( data & 0x01 );
                return;
            }

            m_PRG_WriteProtected = ( data & 0x40 );
            m_PRG_RAM_Enabled = ( data & 0x80 );
            return;
        }

        //
        // IRQ Control
        //

        if ( addr < 0xE000 )
        {
            if ( isEven )
            {
                m_IRQ.latch = data;
                return;
            }

            m_IRQ.reload = true;
            m_IRQ.value = 0;
            return;
        }

        //
        // IRQ Enable / Disable
        //

        m_IRQ.enabled = ( isEven == false );
    }


    //
    //
    //

    virtual uint8_t PRG_ReadByte( uint16_t addr ) override
    {
        // SRAM
        if ( addr < 0x8000 )
            return m_SRAM[addr - 0x6000];

        if ( addr < 0xA000 )
        {
            if ( m_bankRegister.PRGMode )
                return m_PRG[m_PRG_BankOffsets_Fixed[0] + ( addr - 0x8000 )];

            return m_PRG[m_PRG_BankOffsets_Switchable[0] + ( addr - 0x8000 )];
        }

        // Always switchable
        if ( addr < 0xC000 )
            return m_PRG[m_PRG_BankOffsets_Switchable[1] + ( addr - 0xA000 )];

        if ( addr < 0xE000 )
        {
            if ( m_bankRegister.PRGMode )
                return m_PRG[m_PRG_BankOffsets_Switchable[0] + ( addr - 0xC000 )];

            return m_PRG[m_PRG_BankOffsets_Fixed[0] + ( addr - 0xC000 )];
        }

        // Always fixed to last bank
        return m_PRG[m_PRG_BankOffsets_Fixed[1] + ( addr - 0xE000 )];
    }


    //
    // CHR
    //

    virtual void CHR_WriteByte( uint16_t addr, uint8_t data ) override
    {
        if ( m_bankRegister.CHRMode )
        {
            if ( addr < 0x0400 )
            {
                m_CHR[m_CHR_BankOffsets[2] + addr] = data;
                return;
            }

            if ( addr < 0x0800 )
            {
                m_CHR[m_CHR_BankOffsets[3] + ( addr - 0x0400 )] = data;
                return;
            }

            if ( addr < 0x0C00 )
            {
                m_CHR[m_CHR_BankOffsets[4] + ( addr - 0x0800 )] = data;
                return;
            }

            if ( addr < 0x1000 )
            {
                m_CHR[m_CHR_BankOffsets[5] + ( addr - 0x0C00 )] = data;
                return;
            }

            if ( addr < 0x1800 )
            {
                m_CHR[m_CHR_BankOffsets[0] + ( addr - 0x1000 )] = data;
                return;
            }

            m_CHR[m_CHR_BankOffsets[1] + ( addr - 0x1800 )] = data;
        }
        else
        {
            if ( addr < 0x0800 )
            {
                m_CHR[m_CHR_BankOffsets[0] + addr] = data;
                return;
            }

            if ( addr < 0x1000 )
            {
                m_CHR[m_CHR_BankOffsets[1] + ( addr - 0x0800 )] = data;
                return;
            }

            if ( addr < 0x1400 )
            {
                m_CHR[m_CHR_BankOffsets[2] + ( addr - 0x1000 )] = data;
                return;
            }

            if ( addr < 0x1800 )
            {
                m_CHR[m_CHR_BankOffsets[3] + ( addr - 0x1400 )] = data;
                return;
            }

            if ( addr < 0x1C00 )
            {
                m_CHR[m_CHR_BankOffsets[4] + ( addr - 0x1800 )] = data;
                return;
            }

            m_CHR[m_CHR_BankOffsets[5] + ( addr - 0x1C00 )] = data;
        }
    }

    virtual uint8_t CHR_ReadByte( uint16_t addr ) override
    {
        if ( m_bankRegister.CHRMode )
        {
            if ( addr < 0x0400 )
                return m_CHR[m_CHR_BankOffsets[2] + ( addr % 0x0400 )];

            if ( addr < 0x0800 )
                return m_CHR[m_CHR_BankOffsets[3] + ( addr % 0x0400 )];

            if ( addr < 0x0C00 )
                return m_CHR[m_CHR_BankOffsets[4] + ( addr % 0x0400 )];

            if ( addr < 0x1000 )
                return m_CHR[m_CHR_BankOffsets[5] + ( addr % 0x0400 )];

            if ( addr < 0x1800 )
                return m_CHR[m_CHR_BankOffsets[0] + ( addr % 0x0800 )];

            return m_CHR[m_CHR_BankOffsets[1] + ( addr % 0x0800 )];
        }
        else
        {
            if ( addr < 0x0800 )
                return m_CHR[m_CHR_BankOffsets[0] + ( addr % 0x0800 )];

            if ( addr < 0x1000 )
                return m_CHR[m_CHR_BankOffsets[1] + ( addr % 0x0800 )];

            if ( addr < 0x1400 )
                return m_CHR[m_CHR_BankOffsets[2] + ( addr % 0x0400 )];

            if ( addr < 0x1800 )
                return m_CHR[m_CHR_BankOffsets[3] + ( addr % 0x0400 )];

            if ( addr < 0x1C00 )
                return m_CHR[m_CHR_BankOffsets[4] + ( addr % 0x0400 )];

            return m_CHR[m_CHR_BankOffsets[5] + ( addr % 0x0400 )];
        }
    }


    //
    // SRAM
    //

private:
    uint8_t m_SRAM[0x2000]; // $6000-$8000

public:
    virtual void SaveSRAM( const char *filename ) override
    {
        SerializeSRAM( m_SRAM, ARRAYSIZE( m_SRAM ), filename, Serialize::ToDisk );
    }

    virtual void LoadSRAM( const char *filename ) override
    {
        SerializeSRAM( m_SRAM, ARRAYSIZE( m_SRAM ), filename, Serialize::FromDisk );
    }
};

#endif // MAPPER_MMC3_H