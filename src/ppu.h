#ifndef PPU_H
#define PPU_H

#include "memory_maps.h"
#include "cpu.h"

struct SDL_Texture;
struct nes_ntsc_t;

const uint8_t BIT_Status_VBlank = 1 << 7;
const uint8_t BIT_Status_Sprite0Hit = 1 << 6;
const uint8_t BIT_Status_SpriteOverflow = 1 << 5;

extern void _PPU_A12_Changed( bool newState );
extern uint16_t _PPU_GetCycle();
extern uint16_t _PPU_GetScanline();

struct Color_t
{
    uint8_t r, g, b;
};

extern Color_t k_Palettes[64];

union Word_t
{
    uint16_t w;
    struct
    {
        // Little endian!
        uint8_t l;
        uint8_t h;
    };
};

class PPU
{
public:
    PPU();
    ~PPU();

    void Run( uint8_t numCycles );
    void DisplayTiles();

    uint16_t GetCurrentScanline() const
    {
        return m_frameCycles / k_CyclesPerScanline;
    }

    uint16_t GetCurrentCycle() const
    {
        return m_frameCycles % k_CyclesPerScanline;
    }

    //
    // Core memory
    //

public:
    inline uint16_t TranslateAddress( uint16_t addr )
    {
        uint16_t tAddr = addr;

        // Nametable meta-mirror
        if ( tAddr >= 0x3000 && tAddr < 0x3F00 )
            tAddr = 0x2000 + ( addr % 0x0400 );

        // Mapper's range
        if ( addr < 0x3000 )
            return gMapper->TranslateAddress( addr );

        // Palette RAM indices mirror
        if ( tAddr >= 0x3F20 && tAddr <= 0x3FFF )
            tAddr = 0x3F00 + ( tAddr % 0x20 );

        // Palette RAM mirrors
        switch ( tAddr )
        {
            case 0x3F10:
            case 0x3F14:
            case 0x3F18:
            case 0x3F1C:
                return tAddr - 0x0010;
        }

        return tAddr;
    }

    inline void WriteByte( uint16_t addr, uint8_t data )
    {
        if ( addr < 0x2000 )
        {
            gMapper->CHR_WriteByte( addr, data );
            return;
        }

        const uint16_t tAddr = ( TranslateAddress( addr ) % 0x4000 );
        m_Memory[tAddr] = data;

        // Catch a palette update and refresh our cached data
        if ( tAddr >= 0x3F00 )
        {
            if ( tAddr == 0x3F00 )
            {
                // Universal background color changed
                m_refreshPalette_Nametable = true;
                m_refreshPalette_Sprites = true;
            }
            else if ( tAddr >= 0x3F11 )
            {
                m_refreshPalette_Sprites = true;
            }
            else
            {
                m_refreshPalette_Nametable = true;
            }
        }
    }

    inline uint8_t ReadByte( uint16_t addr )
    {
        if ( addr < 0x2000 )
            return gMapper->CHR_ReadByte( addr );

        return m_Memory[TranslateAddress( addr )];
    }

    inline uint16_t ReadWord( uint16_t addr )
    {
        return static_cast<uint16_t>( ( ReadByte( addr + 1 ) << 8 ) | ReadByte( addr ) );
    }

private:
    uint8_t m_Memory[16 * 1024];
    friend class CPU;

    //
    // OAM
    //
public:
    void OAM_WriteAddress( uint8_t address )
    {
        m_OAM_Offset = address;
    }

    uint8_t OAM_ReadData()
    {
        uint8_t *raw = reinterpret_cast<uint8_t *>( m_OAM );
        return raw[m_OAM_Offset];
    }

    void OAM_WriteData( uint8_t data )
    {
        uint8_t *raw = reinterpret_cast<uint8_t *>( m_OAM );
        raw[m_OAM_Offset++] = data;
    }

    void OAM_DMA( uint8_t upperAddress )
    {
        // Penalize the CPU by 513 cycles (+1 on odd CPU cycles)
        const uint64_t cycle = gCPU->GetNumCyclesElapsed();
        gLogger->Log( "[Sprite DMA Started: %llu]\n", cycle );
        gCPU->AddCycles( ( cycle % 2 ) ? 514 : 513 );

        const uint16_t addr = ( upperAddress << 8 ) & ( 0xFF00u );
        memcpy( m_OAM, gCPU->m_Memory + addr, 256 );

        const uint64_t finishCycle = gCPU->GetNumCyclesElapsed();
        gLogger->Log( "[Sprite DMA Ended: %llu]\n", finishCycle );
    }

private:
    uint8_t m_OAM_Offset = 0;
    struct OAMEntry_t
    {
        uint8_t ypos;
        struct TileIndex_t
        {
            union
            {
                uint8_t index;
                struct
                {
                    uint8_t bank : 1;
                    uint8_t number : 7;
                };
            };
        } tileIndex;
        struct Attributes_t
        {
            uint8_t palette : 2;
            uint8_t padding : 3;
            uint8_t priority : 1;
            bool flipHorz : 1;
            bool flipVert : 1;
        } attributes;
        uint8_t xpos;
    };

    OAMEntry_t m_OAM[64];

    // Sprites to be rendered on the next frame
    OAMEntry_t m_SelectedSprites[64];
    size_t m_numSelectedSprites = 0;
    int m_isSpriteZero = false;


    //
    // Control
    //

public:
    uint8_t ReadControl()
    {
        return m_control;
    }

    void WriteControl( uint8_t data )
    {
        uint8_t oldState = m_control;
        m_control = data;

        m_VRAM.T.nametable = ( data & 0x03 );

        if ( !( oldState & ( 1 << 7 ) ) && !!( data & ( 1 << 7 ) ) )
        {
            gCPU->SignalNMIPending();
            m_SignaledNMI = true;
        }
    }

    bool IsNMIDisabled() const
    {
        return !( m_control & ( 1 << 7 ) );
    }

    bool UsingTallSprites() const
    {
        return !!( m_control & ( 1 << 5 ) );
    }

    uint8_t GetIncrementStep() const
    {
        return ( m_control & ( 1 << 2 ) ) ? 32 : 1;
    }

    uint16_t GetBaseNametableAddress() const
    {
        return 0x2000 + ( ( m_control & 0x03 ) * 0x0400 );
    }

    uint16_t GetTileAddress() const
    {
        return ( m_control & ( 1 << 4 ) ) ? 0x1000u : 0x0000u;
    }

    uint16_t GetSpriteTileAddress() const
    {
        return ( m_control & ( 1 << 3 ) ) ? 0x1000u : 0x0000u;
    }

private:
    uint8_t m_control = 0x00;

    //
    // Mask
    //

public:
    uint8_t ReadMask()
    {
        return m_mask;
    }

    void WriteMask( uint8_t data )
    {
        m_mask = data;
    }

    bool IsGrayscaleMode() const
    {
        return ( m_mask & ( 1 << 0 ) );
    }

    bool IsLeftColumnBackgroundEnabled() const
    {
        return ( m_mask & ( 1 << 1 ) );
    }

    bool IsLeftColumnSpriteEnabled() const
    {
        return ( m_mask & ( 1 << 2 ) );
    }

    bool IsBackgroundHidden() const
    {
        return !( m_mask & ( 1 << 3 ) );
    }

    bool IsSpritesHidden() const
    {
        return !( m_mask & ( 1 << 4 ) );
    }

    inline bool IsRendering() const
    {
        return ( !IsBackgroundHidden() || !IsSpritesHidden() );
    }

private:
    uint8_t m_mask = 0x0006; // Default to left-column enabled


    //
    // Scroll
    //

public:
    void WriteScroll( uint8_t data )
    {
        if ( m_VRAM.W )
        {
            m_VRAM.T.fineY = ( data & 0x07 );
            m_VRAM.T.Y = ( data >> 3 );
        }
        else
        {
            m_VRAM.fineX = ( data & 0x07 );
            m_VRAM.T.X = ( data >> 3 );
        }

        m_VRAM.W = !m_VRAM.W;
    }

    //
    // Status
    //

public:
    uint8_t ReadStatus()
    {
        m_VRAM.W = false;

        // Reset the data write
        m_addressPortLatchHigh = true;
        uint8_t currentStatus = m_status;

        // Special case to emulate hardware issue
        const int diff = m_frameCycles - m_lastStatusReadCycle;
        if ( diff == 0 || diff == 1 )
        {
            currentStatus &= ~BIT_Status_VBlank;
        }

        m_lastStatusReadCycle = m_frameCycles;
        m_status &= ~BIT_Status_VBlank;
        return currentStatus;
    }

    void SetVBlank()
    {
        m_status |= BIT_Status_VBlank;
    }

    bool IsVBlankFlagged() const
    {
        return !!( m_status & BIT_Status_VBlank );
    }

    void ClearVBlank()
    {
        m_status &= ~BIT_Status_VBlank;
    }


    bool IsSpriteOverflow() const
    {
        return !!( m_status & BIT_Status_SpriteOverflow );
    }

    void SetSpriteOverflow()
    {
        m_status |= BIT_Status_SpriteOverflow;
    }

    void ClearSpriteOverflow()
    {
        m_status &= ~BIT_Status_SpriteOverflow;
    }

    bool IsSpriteZeroHit() const
    {
        return !!( m_status & BIT_Status_Sprite0Hit );
    }

    void FlagZeroSpriteHit()
    {
        m_status |= BIT_Status_Sprite0Hit;
    }

    void ClearZeroSpriteHit()
    {
        m_status &= ~BIT_Status_Sprite0Hit;
    }

private:
    uint8_t m_status = 0x00;
    int m_lastStatusReadCycle = 0;

    //
    // Data
    //

public:
    uint8_t ReadData()
    {
        const uint8_t result = m_addressPortDataLatch;
        m_addressPortDataLatch = ReadByte( m_addressPort.w );
        m_addressPort.w += GetIncrementStep();
        return result;
    }

    void WriteData( uint8_t data )
    {
        // Store + increment
        // FIXME: Use VRAM here instead!
        WriteByte( m_addressPort.w, data );
        m_addressPort.w += GetIncrementStep();
    }

    void WriteAddress( uint8_t data )
    {
        if ( m_VRAM.W )
        {
            m_VRAM.T.value = ( m_VRAM.T.value & 0xFF00 ) | data;
            m_VRAM.V.value = m_VRAM.T.value;
        }
        else
        {
            m_VRAM.T.value = ( m_VRAM.T.value & 0x00FF ) | ( data << 8 );
            m_VRAM.T.value &= ~( 1 << 15 );
        }

        m_VRAM.W = !m_VRAM.W;

        if ( m_addressPortLatchHigh )
        {
            m_addressPort.h = data;
        }
        else
        {
            m_addressPort.l = data;
        }

        m_addressPortLatchHigh = !m_addressPortLatchHigh;
    }

private:
    Word_t m_addressPort = { 0x0000 };
    bool m_addressPortLatchHigh = true;
    uint8_t m_addressPortDataLatch = 0x00;

public:
    bool inVBlank() const
    {
        const uint16_t currentScanline = m_frameCycles / k_CyclesPerScanline;
        return ( currentScanline >= k_ScanlinesBeforeVBlank );
    }

    //
    // Frame data
    //

public:
    SDL_Texture *GetFramebuffer() const
    {
        return m_framebuffer;
    }

private:
    void RenderNameTable( const uint16_t scanline, const uint16_t currentPixel );

    void SelectSprites( const uint16_t scanline );
    void RenderSprites( const uint16_t scanline, const uint16_t currentPixel );

    SDL_Texture *m_framebuffer = nullptr;
    uint8_t *m_pixels = nullptr;
    int m_pitch = 0;

    bool m_SignaledNMI = false;
    uint64_t m_frameNumber = 0;

private:
    // yyy NN YYYYY XXXXX
    // ||| || ||||| +++++-- coarse X scroll
    // ||| || +++++-------- coarse Y scroll
    // ||| ++-------------- nametable select
    // +++----------------- fine Y scroll

    struct VRegister_t
    {
        union
        {
            uint16_t value;
            struct
            {
                uint16_t X : 5, Y : 5, nametable : 2, fineY : 3;
            };
        };
    };

    struct VRAMInfo_t
    {
        VRegister_t V, T;
        uint8_t fineX : 3;
        bool W;
    } m_VRAM;

    inline void incrementY()
    {
        if ( m_VRAM.V.fineY < 7 )
        {
            ++m_VRAM.V.fineY;
            return;
        }

        m_VRAM.V.fineY = 0;
        if ( m_VRAM.V.Y == 29 )
        {
            m_VRAM.V.Y = 0;
            if ( gMapper->GetMirrorMode() == MirrorMode::Horizontal )
            {
                m_VRAM.V.value ^= 0x0800;
                _PPU_A12_Changed( !!( m_VRAM.V.value & 0x0800 ) );
            }
            return;
        }

        if ( m_VRAM.V.Y == 31 )
        {
            m_VRAM.V.Y = 0;
            return;
        }

        m_VRAM.V.Y++;
    }

    inline void incrementX()
    {
        const uint16_t currentPixel = m_frameCycles % k_CyclesPerScanline;
        if ( ( m_VRAM.fineX + currentPixel ) % 8 )
            return;

        if ( m_VRAM.V.X == 31 )
        {
            m_VRAM.V.X = 0;
            if ( gMapper->GetMirrorMode() == MirrorMode::Vertical )
            {
                m_VRAM.V.value ^= 0x0400;
            }
            return;
        }

        ++m_VRAM.V.X;
    }

    //
    // Background mask
    //

private:
    uint8_t m_backgroundMask[gNativeWidth * gNativeHeight];

    //
    // Timing
    //

public:
    double GetRenderDuration() const
    {
        return m_duration;
    }

private:
    double m_duration = 0.0;

public:
    SDL_Texture *GetCHRPage();

private:
    SDL_Texture *m_CHRPage = nullptr;

    //
    // Frame timing
    //

private:
    void AdvanceCycle()
    {
        const uint16_t currentPixel = m_frameCycles % k_CyclesPerScanline;

        // Deal with vertical advance
        if ( currentPixel == 256 )
        {
            incrementY();
        }
        else if ( currentPixel == 257 )
        {
            if ( IsRendering() )
            {
                // Copy horizontal components over from the temporary register
                m_VRAM.V.X = m_VRAM.T.X;

                if ( m_VRAM.T.value & 0x0400 )
                {
                    m_VRAM.V.value |= 0x0400;
                }
                else
                {
                    m_VRAM.V.value &= ~( 0x0400 );
                }
            }
        }

        // Move ahead
        m_frameCycles = ( m_frameCycles + 1 ) % k_CyclesPerFrame;

        // Only occurs at certain times
        if ( currentPixel <= 256 || currentPixel >= 344 )
        {
            incrementX();
        }
    }

    int m_frameCycles = 0;

    //
    // Palette caching
    //

private:
    enum class PaletteType
    {
        Nametable,
        Sprites
    };

    void UpdatePaletteIndices( PaletteType type );
    uint8_t m_paletteIndices_Nametable[4][4];
    uint8_t m_paletteIndices_Sprites[4][4];
    bool m_refreshPalette_Nametable = true;
    bool m_refreshPalette_Sprites = true;

    //
    // NTSC Filter
    //
public:
    void RenderNTSCFilter();

private:
    void DoubleOutputHeight( void );
    nes_ntsc_t *m_NTSC = nullptr;
};


#endif // PPU_H