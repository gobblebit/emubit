/*
    6502 CPU emulation
*/

#include "base.h"

#include <ctime>
#include <limits>

#include "thirdparty/nes_snd_emu/Simple_Apu.h"

#include "apu.h"
#include "ppu.h"
#include "controller.h"

#include "cpu.h"

extern bool gSoundEnabled;

extern void _SignalExit( const char *reason = nullptr );

int _ReadDMC( void *, cpu_addr_t addr )
{
    return gCPU->ReadByte( addr );
}

extern cpu_time_t _CPU_TimeElapsed();

// clang-format off
const std::map<uint8_t, const char *> m_OpCodeLookup = {
    { 0x00, "BRK" }, { 0x01, "ORA" }, { 0x05, "ORA" }, { 0x06, "ASL" }, { 0x08, "PHP" },
    { 0x09, "ORA" }, { 0x0A, "ASL" }, { 0x0D, "ORA" }, { 0x0E, "ASL" }, { 0x10, "BPL" },
    { 0x11, "ORA" }, { 0x15, "ORA" }, { 0x16, "ASL" }, { 0x18, "CLC" }, { 0x19, "ORA" },
    { 0x1D, "ORA" }, { 0x1E, "ASL" }, { 0x20, "JSR" }, { 0x21, "AND" }, { 0x24, "BIT" },
    { 0x25, "AND" }, { 0x26, "ROL" }, { 0x28, "PLP" }, { 0x29, "AND" }, { 0x2A, "ROL" },
    { 0x2C, "BIT" }, { 0x2D, "AND" }, { 0x2E, "ROL" }, { 0x30, "BMI" }, { 0x31, "AND" },
    { 0x35, "AND" }, { 0x36, "ROL" }, { 0x38, "SEC" }, { 0x39, "AND" }, { 0x3D, "AND" },
    { 0x3E, "ROL" }, { 0x40, "RTI" }, { 0x41, "EOR" }, { 0x45, "EOR" }, { 0x46, "LSR" },
    { 0x48, "PHA" }, { 0x49, "EOR" }, { 0x4A, "LSR" }, { 0x4C, "JMP" }, { 0x4D, "EOR" },
    { 0x4E, "LSR" }, { 0x50, "BVC" }, { 0x51, "EOR" }, { 0x55, "EOR" }, { 0x56, "LSR" },
    { 0x58, "CLI" }, { 0x59, "EOR" }, { 0x5D, "EOR" }, { 0x5E, "LSR" }, { 0x60, "RTS" },
    { 0x61, "ADC" }, { 0x65, "ADC" }, { 0x66, "ROR" }, { 0x68, "PLA" }, { 0x69, "ADC" },
    { 0x6A, "ROR" }, { 0x6C, "JMP" }, { 0x6D, "ADC" }, { 0x6E, "ROR" }, { 0x70, "BVS" },
    { 0x71, "ADC" }, { 0x75, "ADC" }, { 0x76, "ROR" }, { 0x78, "SEI" }, { 0x79, "ADC" },
    { 0x7D, "ADC" }, { 0x7E, "ROR" }, { 0x81, "STA" }, { 0x84, "STY" }, { 0x85, "STA" },
    { 0x86, "STX" }, { 0x88, "DEY" }, { 0x8A, "TXA" }, { 0x8C, "STY" }, { 0x8D, "STA" },
    { 0x8E, "STX" }, { 0x90, "BCC" }, { 0x91, "STA" }, { 0x94, "STY" }, { 0x95, "STA" },
    { 0x96, "STX" }, { 0x98, "TYA" }, { 0x99, "STA" }, { 0x9A, "TXS" }, { 0x9D, "STA" },
    { 0xA0, "LDY" }, { 0xA1, "LDA" }, { 0xA2, "LDX" }, { 0xA4, "LDY" }, { 0xA5, "LDA" },
    { 0xA6, "LDX" }, { 0xA8, "TAY" }, { 0xA9, "LDA" }, { 0xAA, "TAX" }, { 0xAC, "LDY" },
    { 0xAD, "LDA" }, { 0xAE, "LDX" }, { 0xB0, "BCS" }, { 0xB1, "LDA" }, { 0xB4, "LDY" },
    { 0xB5, "LDA" }, { 0xB6, "LDX" }, { 0xB8, "CLV" }, { 0xB9, "LDA" }, { 0xBA, "TSX" },
    { 0xBC, "LDY" }, { 0xBD, "LDA" }, { 0xBE, "LDX" }, { 0xC0, "CPY" }, { 0xC1, "CMP" },
    { 0xC4, "CPY" }, { 0xC5, "CMP" }, { 0xC6, "DEC" }, { 0xC8, "INY" }, { 0xC9, "CMP" },
    { 0xCA, "DEX" }, { 0xCC, "CPY" }, { 0xCD, "CMP" }, { 0xCE, "DEC" }, { 0xD0, "BNE" },
    { 0xD1, "CMP" }, { 0xD5, "CMP" }, { 0xD6, "DEC" }, { 0xD8, "CLD" }, { 0xD9, "CMP" },
    { 0xDD, "CMP" }, { 0xDE, "DEC" }, { 0xE0, "CPX" }, { 0xE1, "SBC" }, { 0xE4, "CPX" },
    { 0xE5, "SBC" }, { 0xE6, "INC" }, { 0xE8, "INX" }, { 0xE9, "SBC" }, { 0xEA, "NOP" },
    { 0xEC, "CPX" }, { 0xED, "SBC" }, { 0xEE, "INC" }, { 0xF0, "BEQ" }, { 0xF1, "SBC" },
    { 0xF5, "SBC" }, { 0xF6, "INC" }, { 0xF8, "SED" }, { 0xF9, "SBC" }, { 0xFD, "SBC" },
    { 0xFE, "INC" }
};
// clang-format on

CPU::CPU()
{
    PopulateOpcodes();
    RandomizeMemory();
}

void CPU::RandomizeMemory()
{
    uint8_t *writePos = (uint8_t *)m_Memory;
    std::srand( static_cast<unsigned int>( std::time( 0 ) ) );
    for ( int i = 0; i < ARRAYSIZE( m_Memory ); ++i )
    {
        writePos[i] = std::rand() % 0xFF;
    }
}

void CPU::PopulateOpcodes()
{
    //
    // Storage
    //

    //
    // LDA (Immediate)
    //
    m_Opcodes[0xA9] = [this]() -> int16_t {
        LDA( immediate() );
        m_Cycles += 2;
        return 1;
    };

    // LDA (Zero Page)
    m_Opcodes[0xA5] = [this]() -> int16_t {
        LDA( ReadByte( zeroPage() ) );
        m_Cycles += 3;
        return 1;
    };

    // LDA (Zero Page, X)
    m_Opcodes[0xB5] = [this]() -> int16_t {
        LDA( ReadByte( zeroPageX() ) );
        m_Cycles += 4;
        return 1;
    };

    // LDA (Absolute)
    m_Opcodes[0xAD] = [this]() -> int16_t {
        LDA( ReadByte( absolute() ) );
        m_Cycles += 4;
        return 2;
    };

    // LDA (Absolute, X)
    m_Opcodes[0xBD] = [this]() -> int16_t {
        LDA( ReadByte( absoluteX() ) );
        m_Cycles += 4;
        AddPageBoundaryPenalty();
        return 2;
    };

    // LDA (Absolute, Y)
    m_Opcodes[0xB9] = [this]() -> int16_t {
        LDA( ReadByte( absoluteY() ) );
        m_Cycles += 4;
        AddPageBoundaryPenalty();
        return 2;
    };

    // LDA (Indirect, X)
    m_Opcodes[0xA1] = [this]() -> int16_t {
        LDA( ReadByte( indirectX() ) );
        m_Cycles += 6;
        return 1;
    };

    // LDA (Indirect, Y)
    m_Opcodes[0xB1] = [this]() -> int16_t {
        LDA( ReadByte( indirectY() ) );
        m_Cycles += 5;
        AddPageBoundaryPenalty();
        return 1;
    };

    //
    // LDX (Immediate)
    //
    m_Opcodes[0xA2] = [this]() -> int16_t {
        LDX( immediate() );
        m_Cycles += 2;
        return 1;
    };

    // LDX (Zero Page)
    m_Opcodes[0xA6] = [this]() -> int16_t {
        LDX( ReadByte( zeroPage() ) );
        m_Cycles += 3;
        return 1;
    };

    // LDX (Zero Page, Y)
    m_Opcodes[0xB6] = [this]() -> int16_t {
        LDX( ReadByte( zeroPageY() ) );
        m_Cycles += 4;
        return 1;
    };

    // LDX (Absolute)
    m_Opcodes[0xAE] = [this]() -> int16_t {
        LDX( ReadByte( absolute() ) );
        m_Cycles += 4;
        return 2;
    };

    // LDX (Absolute, Y)
    m_Opcodes[0xBE] = [this]() -> int16_t {
        LDX( ReadByte( absoluteY() ) );
        m_Cycles += 4;
        AddPageBoundaryPenalty();
        return 2;
    };

    //
    // LDY (Immediate)
    //
    m_Opcodes[0xA0] = [this]() -> int16_t {
        LDY( immediate() );
        m_Cycles += 2;
        return 1;
    };

    // LDY (Zero Page)
    m_Opcodes[0xA4] = [this]() -> int16_t {
        LDY( ReadByte( zeroPage() ) );
        m_Cycles += 3;
        return 1;
    };

    // LDY (Zero Page, X)
    m_Opcodes[0xB4] = [this]() -> int16_t {
        LDY( ReadByte( zeroPageX() ) );
        m_Cycles += 4;
        return 1;
    };

    // LDY (Absolute)
    m_Opcodes[0xAC] = [this]() -> int16_t {
        LDY( ReadByte( absolute() ) );
        m_Cycles += 4;
        return 2;
    };

    // LDY (Absolute, X)
    m_Opcodes[0xBC] = [this]() -> int16_t {
        LDY( ReadByte( absoluteX() ) );
        m_Cycles += 4;
        AddPageBoundaryPenalty();
        return 2;
    };

    //
    // STA (Zero Page)
    //
    m_Opcodes[0x85] = [this]() -> int16_t {
        WriteByte( zeroPage(), m_Registers.A );
        m_Cycles += 3;
        return 1;
    };

    // STA (Zero Page, X)
    m_Opcodes[0x95] = [this]() -> int16_t {
        WriteByte( zeroPageX(), m_Registers.A );
        m_Cycles += 4;
        return 1;
    };

    // STA (Absolute)
    m_Opcodes[0x8D] = [this]() -> int16_t {
        WriteByte( absolute(), m_Registers.A );
        m_Cycles += 4;
        return 2;
    };

    // STA (Absolute, X)
    m_Opcodes[0x9D] = [this]() -> int16_t {
        WriteByte( absoluteX(), m_Registers.A );
        m_Cycles += 5;
        return 2;
    };

    // STA (Absolute, Y)
    m_Opcodes[0x99] = [this]() -> int16_t {
        WriteByte( absoluteY(), m_Registers.A );
        m_Cycles += 5;
        return 2;
    };

    // STA (Indirect, X)
    m_Opcodes[0x81] = [this]() -> int16_t {
        WriteByte( indirectX(), m_Registers.A );
        m_Cycles += 6;
        return 1;
    };

    // STA (Indirect, Y)
    m_Opcodes[0x91] = [this]() -> int16_t {
        WriteByte( indirectY(), m_Registers.A );
        m_Cycles += 6;
        return 1;
    };

    //
    // STX (Zero Page)
    //
    m_Opcodes[0x86] = [this]() -> int16_t {
        WriteByte( zeroPage(), m_Registers.X );
        m_Cycles += 3;
        return 1;
    };

    // STX (Zero Page, Y)
    m_Opcodes[0x96] = [this]() -> int16_t {
        WriteByte( zeroPageY(), m_Registers.X );
        m_Cycles += 4;
        return 1;
    };

    // STX (Absolute)
    m_Opcodes[0x8E] = [this]() -> int16_t {
        WriteByte( absolute(), m_Registers.X );
        m_Cycles += 4;
        return 2;
    };

    //
    // STY (Zero Page)
    //
    m_Opcodes[0x84] = [this]() -> int16_t {
        WriteByte( zeroPage(), m_Registers.Y );
        m_Cycles += 3;
        return 1;
    };

    // STY (Zero Page, X)
    m_Opcodes[0x94] = [this]() -> int16_t {
        WriteByte( zeroPageX(), m_Registers.Y );
        m_Cycles += 4;
        return 1;
    };

    // STY (Absolute)
    m_Opcodes[0x8C] = [this]() -> int16_t {
        WriteByte( absolute(), m_Registers.Y );
        m_Cycles += 4;
        return 2;
    };

    //
    // TAX
    //
    m_Opcodes[0xAA] = [this]() -> int16_t {
        m_Registers.X = m_Registers.A;
        status_SetNegative( m_Registers.X );
        status_SetZero( m_Registers.X );
        m_Cycles += 2;
        return 0;
    };

    //
    // TAY
    //
    m_Opcodes[0xA8] = [this]() -> int16_t {
        m_Registers.Y = m_Registers.A;
        status_SetNegative( m_Registers.Y );
        status_SetZero( m_Registers.Y );
        m_Cycles += 2;
        return 0;
    };

    //
    // TSX
    //
    m_Opcodes[0xBA] = [this]() -> int16_t {
        m_Registers.X = m_Registers.S;
        status_SetNegative( m_Registers.X );
        status_SetZero( m_Registers.X );
        m_Cycles += 2;
        return 0;
    };

    //
    // TXA
    //
    m_Opcodes[0x8A] = [this]() -> int16_t {
        m_Registers.A = m_Registers.X;
        status_SetNegative( m_Registers.A );
        status_SetZero( m_Registers.A );
        m_Cycles += 2;
        return 0;
    };

    //
    // TXS
    //
    m_Opcodes[0x9A] = [this]() -> int16_t {
        m_Registers.S = m_Registers.X;
        m_Cycles += 2;
        return 0;
    };

    //
    // TYA
    //
    m_Opcodes[0x98] = [this]() -> int16_t {
        m_Registers.A = m_Registers.Y;
        status_SetNegative( m_Registers.A );
        status_SetZero( m_Registers.A );
        m_Cycles += 2;
        return 0;
    };


    //
    // Math
    //

    //
    // ADC (Immediate)
    //
    m_Opcodes[0x69] = [this]() -> int16_t {
        ADC( immediate() );
        m_Cycles += 2;
        return 1;
    };

    // ADC (Zero Page)
    m_Opcodes[0x65] = [this]() -> int16_t {
        ADC( ReadByte( zeroPage() ) );
        m_Cycles += 3;
        return 1;
    };

    // ADC (Zero Page, X)
    m_Opcodes[0x75] = [this]() -> int16_t {
        ADC( ReadByte( zeroPageX() ) );
        m_Cycles += 4;
        return 1;
    };

    // ADC (Absolute)
    m_Opcodes[0x6D] = [this]() -> int16_t {
        ADC( ReadByte( absolute() ) );
        m_Cycles += 4;
        return 2;
    };

    // ADC (Absolute, X)
    m_Opcodes[0x7D] = [this]() -> int16_t {
        ADC( ReadByte( absoluteX() ) );
        m_Cycles += 4;
        AddPageBoundaryPenalty();
        return 2;
    };

    // ADC (Absolute, Y)
    m_Opcodes[0x79] = [this]() -> int16_t {
        ADC( ReadByte( absoluteY() ) );
        m_Cycles += 4;
        AddPageBoundaryPenalty();
        return 2;
    };

    // ADC (Indirect, X)
    m_Opcodes[0x61] = [this]() -> int16_t {
        ADC( ReadByte( indirectX() ) );
        m_Cycles += 6;
        return 1;
    };

    // ADC (Indirect, Y)
    m_Opcodes[0x71] = [this]() -> int16_t {
        ADC( ReadByte( indirectY() ) );
        m_Cycles += 5;
        AddPageBoundaryPenalty();
        return 1;
    };

    //
    // DEC (Zero Page)
    //
    m_Opcodes[0xC6] = [this]() -> int16_t {
        DEC( zeroPage() );
        m_Cycles += 5;
        return 1;
    };

    // DEC (Zero Page, X)
    m_Opcodes[0xD6] = [this]() -> int16_t {
        DEC( zeroPageX() );
        m_Cycles += 6;
        return 1;
    };

    // DEC (Absolute)
    m_Opcodes[0xCE] = [this]() -> int16_t {
        DEC( absolute() );
        m_Cycles += 6;
        return 2;
    };

    // DEC (Absolute, X)
    m_Opcodes[0xDE] = [this]() -> int16_t {
        DEC( absoluteX() );
        m_Cycles += 7;
        return 2;
    };

    //
    // DEX
    //
    m_Opcodes[0xCA] = [this]() -> int16_t {
        --m_Registers.X;
        status_SetZero( m_Registers.X );
        status_SetNegative( m_Registers.X );
        m_Cycles += 2;
        return 0;
    };

    //
    // DEY
    //
    m_Opcodes[0x88] = [this]() -> int16_t {
        --m_Registers.Y;
        status_SetZero( m_Registers.Y );
        status_SetNegative( m_Registers.Y );
        m_Cycles += 2;
        return 0;
    };

    //
    // INC (Zero Page)
    //
    m_Opcodes[0xE6] = [this]() -> int16_t {
        INC( zeroPage() );
        m_Cycles += 5;
        return 1;
    };

    // INC (Zero Page, X)
    m_Opcodes[0xF6] = [this]() -> int16_t {
        INC( zeroPageX() );
        m_Cycles += 6;
        return 1;
    };

    // INC (Absolute)
    m_Opcodes[0xEE] = [this]() -> int16_t {
        INC( absolute() );
        m_Cycles += 6;
        return 2;
    };

    // INC (Absolute, X)
    m_Opcodes[0xFE] = [this]() -> int16_t {
        INC( absoluteX() );
        m_Cycles += 7;
        return 2;
    };

    //
    // INX
    //
    m_Opcodes[0xE8] = [this]() -> int16_t {
        ++m_Registers.X;
        status_SetZero( m_Registers.X );
        status_SetNegative( m_Registers.X );
        m_Cycles += 2;
        return 0;
    };

    //
    // INY
    //
    m_Opcodes[0xC8] = [this]() -> int16_t {
        ++m_Registers.Y;
        status_SetZero( m_Registers.Y );
        status_SetNegative( m_Registers.Y );
        m_Cycles += 2;
        return 0;
    };

    //
    // SBC (Immediate)
    //
    m_Opcodes[0xE9] = [this]() -> int16_t {
        SBC( immediate() );
        m_Cycles += 2;
        return 1;
    };

    // SBC (Zero Page)
    m_Opcodes[0xE5] = [this]() -> int16_t {
        SBC( ReadByte( zeroPage() ) );
        m_Cycles += 3;
        return 1;
    };

    // SBC (Zero Page, X)
    m_Opcodes[0xF5] = [this]() -> int16_t {
        SBC( ReadByte( zeroPageX() ) );
        m_Cycles += 4;
        return 1;
    };

    // SBC (Absolute)
    m_Opcodes[0xED] = [this]() -> int16_t {
        SBC( ReadByte( absolute() ) );
        m_Cycles += 4;
        return 2;
    };

    // SBC (Absolute, X)
    m_Opcodes[0xFD] = [this]() -> int16_t {
        SBC( ReadByte( absoluteX() ) );
        m_Cycles += 4;
        AddPageBoundaryPenalty();
        return 2;
    };

    // SBC (Absolute, Y)
    m_Opcodes[0xF9] = [this]() -> int16_t {
        SBC( ReadByte( absoluteY() ) );
        m_Cycles += 4;
        AddPageBoundaryPenalty();
        return 2;
    };

    // SBC (Indirect, X)
    m_Opcodes[0xE1] = [this]() -> int16_t {
        SBC( ReadByte( indirectX() ) );
        m_Cycles += 6;
        return 1;
    };

    // SBC (Indirect, Y)
    m_Opcodes[0xF1] = [this]() -> int16_t {
        SBC( ReadByte( indirectY() ) );
        m_Cycles += 5;
        AddPageBoundaryPenalty();
        return 1;
    };

    //
    // Bitwise
    //

    //
    // AND (Immediate)
    //
    m_Opcodes[0x29] = [this]() -> int16_t {
        AND( immediate() );
        m_Cycles += 2;
        return 1;
    };

    // AND (Zero Page)
    m_Opcodes[0x25] = [this]() -> int16_t {
        AND( ReadByte( zeroPage() ) );
        m_Cycles += 3;
        return 1;
    };

    // AND (Zero-page X)
    m_Opcodes[0x35] = [this]() -> int16_t {
        AND( ReadByte( zeroPageX() ) );
        m_Cycles += 4;
        return 1;
    };

    // AND (Absolute)
    m_Opcodes[0x2D] = [this]() -> int16_t {
        AND( ReadByte( absolute() ) );
        m_Cycles += 4;
        return 2;
    };

    // AND (Absolute X)
    m_Opcodes[0x3D] = [this]() -> int16_t {
        AND( ReadByte( absoluteX() ) );
        m_Cycles += 4;
        AddPageBoundaryPenalty();
        return 2;
    };

    // AND (Absolute Y)
    m_Opcodes[0x39] = [this]() -> int16_t {
        AND( ReadByte( absoluteY() ) );
        m_Cycles += 4;
        AddPageBoundaryPenalty();
        return 2;
    };

    // AND (Indirect X)
    m_Opcodes[0x21] = [this]() -> int16_t {
        AND( ReadByte( indirectX() ) );
        m_Cycles += 6;
        return 1;
    };

    // AND (Indirect Y)
    m_Opcodes[0x31] = [this]() -> int16_t {
        AND( ReadByte( indirectY() ) );
        m_Cycles += 5;
        AddPageBoundaryPenalty();
        return 1;
    };

    //
    // ASL (Accumulator)
    //
    m_Opcodes[0x0A] = [this]() -> int16_t {
        m_Registers.A = _ASL( m_Registers.A );
        m_Cycles += 2;
        return 0;
    };

    // ASL (Zero Page)
    m_Opcodes[0x06] = [this]() -> int16_t {
        ASL( zeroPage() );
        m_Cycles += 5;
        return 1;
    };

    // ASL (Zero page, X)
    m_Opcodes[0x16] = [this]() -> int16_t {
        ASL( zeroPageX() );
        m_Cycles += 6;
        return 1;
    };

    // ASL (Absolute)
    m_Opcodes[0x0E] = [this]() -> int16_t {
        ASL( absolute() );
        m_Cycles += 6;
        return 2;
    };

    // ASL (Absolute X)
    m_Opcodes[0x1E] = [this]() -> int16_t {
        ASL( absoluteX() );
        m_Cycles += 7;
        return 2;
    };

    //
    // BIT (Zero Page)
    //
    m_Opcodes[0x24] = [this]() -> int16_t {
        BIT( ReadByte( zeroPage() ) );
        m_Cycles += 3;
        return 1;
    };

    // BIT (Absolute)
    m_Opcodes[0x2C] = [this]() -> int16_t {
        BIT( ReadByte( absolute() ) );
        m_Cycles += 4;
        return 2;
    };

    //
    // EOR (Immediate)
    //
    m_Opcodes[0x49] = [this]() -> int16_t {
        EOR( immediate() );
        m_Cycles += 2;
        return 1;
    };

    // EOR (Zero page)
    m_Opcodes[0x45] = [this]() -> int16_t {
        EOR( ReadByte( zeroPage() ) );
        m_Cycles += 3;
        return 1;
    };

    // EOR (Zero Page X)
    m_Opcodes[0x55] = [this]() -> int16_t {
        EOR( ReadByte( zeroPageX() ) );
        m_Cycles += 4;
        return 1;
    };

    // EOR (Absolute)
    m_Opcodes[0x4D] = [this]() -> int16_t {
        EOR( ReadByte( absolute() ) );
        m_Cycles += 4;
        return 2;
    };

    // EOR (Absolute X)
    m_Opcodes[0x5D] = [this]() -> int16_t {
        EOR( ReadByte( absoluteX() ) );
        m_Cycles += 4;
        AddPageBoundaryPenalty();
        return 2;
    };

    // EOR (Absolute Y)
    m_Opcodes[0x59] = [this]() -> int16_t {
        EOR( ReadByte( absoluteY() ) );
        m_Cycles += 4;
        AddPageBoundaryPenalty();
        return 2;
    };

    // EOR (Indirect X)
    m_Opcodes[0x41] = [this]() -> int16_t {
        EOR( ReadByte( indirectX() ) );
        m_Cycles += 6;
        return 1;
    };

    // EOR (Indirect Y)
    m_Opcodes[0x51] = [this]() -> int16_t {
        EOR( ReadByte( indirectY() ) );
        m_Cycles += 5;
        AddPageBoundaryPenalty();
        return 1;
    };

    //
    // LSR (Accumulator)
    //
    m_Opcodes[0x4A] = [this]() -> int16_t {
        m_Registers.A = _LSR( m_Registers.A );
        m_Cycles += 2;
        return 0;
    };

    // LSR (Zero page)
    m_Opcodes[0x46] = [this]() -> int16_t {
        LSR( zeroPage() );
        m_Cycles += 5;
        return 1;
    };

    // LSR (Zero page X)
    m_Opcodes[0x56] = [this]() -> int16_t {
        LSR( zeroPageX() );
        m_Cycles += 6;
        return 1;
    };

    // LSR (Absolute)
    m_Opcodes[0x4E] = [this]() -> int16_t {
        LSR( absolute() );
        m_Cycles += 6;
        return 2;
    };

    // LSR (Absolute, X)
    m_Opcodes[0x5E] = [this]() -> int16_t {
        LSR( absoluteX() );
        m_Cycles += 7;
        return 2;
    };

    //
    // ORA (Immediate)
    //
    m_Opcodes[0x09] = [this]() -> int16_t {
        ORA( immediate() );
        m_Cycles += 2;
        return 1;
    };

    // ORA (Zero Page)
    m_Opcodes[0x05] = [this]() -> int16_t {
        ORA( ReadByte( zeroPage() ) );
        m_Cycles += 3;
        return 1;
    };

    // ORA (Zero-page X)
    m_Opcodes[0x15] = [this]() -> int16_t {
        ORA( ReadByte( zeroPageX() ) );
        m_Cycles += 4;
        return 1;
    };

    // ORA (Absolute)
    m_Opcodes[0x0D] = [this]() -> int16_t {
        ORA( ReadByte( absolute() ) );
        m_Cycles += 4;
        return 2;
    };

    // ORA (Absolute X)
    m_Opcodes[0x1D] = [this]() -> int16_t {
        ORA( ReadByte( absoluteX() ) );
        m_Cycles += 4;
        AddPageBoundaryPenalty();
        return 2;
    };

    // ORA (Absolute Y)
    m_Opcodes[0x19] = [this]() -> int16_t {
        ORA( ReadByte( absoluteY() ) );
        m_Cycles += 4;
        AddPageBoundaryPenalty();
        return 2;
    };

    // ORA (Indirect X)
    m_Opcodes[0x01] = [this]() -> int16_t {
        ORA( ReadByte( indirectX() ) );
        m_Cycles += 6;
        return 1;
    };

    // ORA (Indirect Y)
    m_Opcodes[0x11] = [this]() -> int16_t {
        ORA( ReadByte( indirectY() ) );
        m_Cycles += 5;
        AddPageBoundaryPenalty();
        return 1;
    };

    //
    // ROL (Accumulator)
    //
    m_Opcodes[0x2A] = [this]() -> int16_t {
        m_Registers.A = _ROL( m_Registers.A );
        m_Cycles += 2;
        return 0;
    };

    // ROL (Zero Page)
    m_Opcodes[0x26] = [this]() -> int16_t {
        ROL( zeroPage() );
        m_Cycles += 5;
        return 1;
    };

    // ROL (Zero Page, X)
    m_Opcodes[0x36] = [this]() -> int16_t {
        ROL( zeroPageX() );
        m_Cycles += 6;
        return 1;
    };

    // ROL (Absolute)
    m_Opcodes[0x2E] = [this]() -> int16_t {
        ROL( absolute() );
        m_Cycles += 6;
        return 2;
    };

    // ROL (Absolute, X)
    m_Opcodes[0x3E] = [this]() -> int16_t {
        ROL( absoluteX() );
        m_Cycles += 7;
        return 2;
    };

    //
    // ROR (Accumulator)
    //
    m_Opcodes[0x6A] = [this]() -> int16_t {
        m_Registers.A = _ROR( m_Registers.A );
        m_Cycles += 2;
        return 0;
    };

    // ROR (Zero Page)
    m_Opcodes[0x66] = [this]() -> int16_t {
        ROR( zeroPage() );
        m_Cycles += 5;
        return 1;
    };

    // ROR (Zero Page, X)
    m_Opcodes[0x76] = [this]() -> int16_t {
        ROR( zeroPageX() );
        m_Cycles += 6;
        return 1;
    };

    // ROR (Absolute)
    m_Opcodes[0x6E] = [this]() -> int16_t {
        ROR( absolute() );
        m_Cycles += 6;
        return 2;
    };

    // ROR (Absolute, X)
    m_Opcodes[0x7E] = [this]() -> int16_t {
        ROR( absoluteX() );
        m_Cycles += 7;
        return 2;
    };


    //
    // Registers
    //

    //
    // CLC
    //
    m_Opcodes[0x18] = [this]() -> int16_t {
        status_SetCarry( false );
        m_Cycles += 2;
        return 0;
    };

    //
    // CLD
    //
    m_Opcodes[0xD8] = [this]() -> int16_t {
        // NOTE: This is an invalid NES command as it didn't support decimal mode!
        status_SetDecimal( false );
        m_Cycles += 2;
        return 0;
    };

    //
    // CLI
    //
    m_Opcodes[0x58] = [this]() -> int16_t {
        SET_BIT( m_Registers.P, BIT_Interrupt, false );
        m_Cycles += 2;
        return 0;
    };

    //
    // CLV
    //
    m_Opcodes[0xB8] = [this]() -> int16_t {
        SET_BIT( m_Registers.P, BIT_Overflow, false );
        m_Cycles += 2;
        return 0;
    };

    //
    // CMP (Immediate)
    //
    m_Opcodes[0xC9] = [this]() -> int16_t {
        CMP( immediate() );
        m_Cycles += 2;
        return 1;
    };

    // CMP (Zero page)
    m_Opcodes[0xC5] = [this]() -> int16_t {
        CMP( ReadByte( zeroPage() ) );
        m_Cycles += 3;
        return 1;
    };

    // CMP (Zero page, X)
    m_Opcodes[0xD5] = [this]() -> int16_t {
        CMP( ReadByte( zeroPageX() ) );
        m_Cycles += 4;
        return 1;
    };

    // CMP (Absolute)
    m_Opcodes[0xCD] = [this]() -> int16_t {
        CMP( ReadByte( absolute() ) );
        m_Cycles += 4;
        return 2;
    };

    // CMP (Absolute, X)
    m_Opcodes[0xDD] = [this]() -> int16_t {
        CMP( ReadByte( absoluteX() ) );
        m_Cycles += 4;
        AddPageBoundaryPenalty();
        return 2;
    };

    // CMP (Absolute, Y)
    m_Opcodes[0xD9] = [this]() -> int16_t {
        CMP( ReadByte( absoluteY() ) );
        m_Cycles += 4;
        AddPageBoundaryPenalty();
        return 2;
    };

    // CMP (Indirect, X)
    m_Opcodes[0xC1] = [this]() -> int16_t {
        CMP( ReadByte( indirectX() ) );
        m_Cycles += 6;
        return 1;
    };

    // CMP (Indirect, Y)
    m_Opcodes[0xD1] = [this]() -> int16_t {
        CMP( ReadByte( indirectY() ) );
        m_Cycles += 5;
        AddPageBoundaryPenalty();
        return 1;
    };

    //
    // CPX (Immediate)
    //
    m_Opcodes[0xE0] = [this]() -> int16_t {
        CPX( immediate() );
        m_Cycles += 2;
        return 1;
    };

    // CPX (Zero Page)
    m_Opcodes[0xE4] = [this]() -> int16_t {
        CPX( ReadByte( zeroPage() ) );
        m_Cycles += 3;
        return 1;
    };

    // CPX (Absolute)
    m_Opcodes[0xEC] = [this]() -> int16_t {
        CPX( ReadByte( absolute() ) );
        m_Cycles += 4;
        return 2;
    };

    //
    // CPY (Immediate)
    //
    m_Opcodes[0xC0] = [this]() -> int16_t {
        CPY( immediate() );
        m_Cycles += 2;
        return 1;
    };

    // CPY (Zero Page)
    m_Opcodes[0xC4] = [this]() -> int16_t {
        CPY( ReadByte( zeroPage() ) );
        m_Cycles += 3;
        return 1;
    };

    // CPY (Absolute)
    m_Opcodes[0xCC] = [this]() -> int16_t {
        CPY( ReadByte( absolute() ) );
        m_Cycles += 4;
        return 2;
    };

    //
    // SEC
    //
    m_Opcodes[0x38] = [this]() -> int16_t {
        status_SetCarry( true );
        m_Cycles += 2;
        return 0;
    };

    //
    // SED
    //
    m_Opcodes[0xF8] = [this]() -> int16_t {
        // NOTE: This is not a valid opcode for the NES CPU!
        status_SetDecimal( true );
        m_Cycles += 2;
        return 0;
    };

    //
    // SEI
    //
    m_Opcodes[0x78] = [this]() -> int16_t {
        SET_BIT( m_Registers.P, BIT_Interrupt, true );
        m_Cycles += 2;
        return 0;
    };


    //
    // Branch
    //

    //
    // BCC
    //
    m_Opcodes[0x90] = [this]() -> int16_t {
        m_Cycles += 2;
        if ( status_HasCarry() )
            return 1;

        // Branch penalty
        ++m_Cycles;

        return ExecuteBranch();
    };

    //
    // BCS
    //
    m_Opcodes[0xB0] = [this]() -> int16_t {
        m_Cycles += 2;
        if ( status_HasCarry() )
        {
            // Branch penalty
            ++m_Cycles;

            return ExecuteBranch();
        }

        return 1;
    };

    //
    // BEQ
    //
    m_Opcodes[0xF0] = [this]() -> int16_t {
        m_Cycles += 2;
        if ( status_HasZero() )
        {
            // Branch penalty
            ++m_Cycles;

            return ExecuteBranch();
        }

        return 1;
    };

    //
    // BMI
    //
    m_Opcodes[0x30] = [this]() -> int16_t {
        m_Cycles += 2;
        if ( status_HasNegative() )
        {
            // Branch penalty
            ++m_Cycles;

            return ExecuteBranch();
        }

        return 1;
    };

    //
    // BNE
    //
    m_Opcodes[0xD0] = [this]() -> int16_t {
        m_Cycles += 2;
        if ( status_HasZero() )
            return 1;

        // Branch penalty
        ++m_Cycles;

        return ExecuteBranch();
    };

    //
    // BPL
    //
    m_Opcodes[0x10] = [this]() -> int16_t {
        m_Cycles += 2;
        if ( status_HasNegative() )
            return 1;

        // Branch penalty
        ++m_Cycles;

        return ExecuteBranch();
    };

    //
    // BVC
    //
    m_Opcodes[0x50] = [this]() -> int16_t {
        m_Cycles += 2;
        if ( status_HasOverflow() )
            return 1;

        // Branch penalty
        ++m_Cycles;

        return ExecuteBranch();
    };

    //
    // BVS
    //
    m_Opcodes[0x70] = [this]() -> int16_t {
        m_Cycles += 2;
        if ( status_HasOverflow() )
        {
            // Branch penalty
            ++m_Cycles;

            return ExecuteBranch();
        }

        return 1;
    };


    //
    // Stack
    //

    //
    // PHA
    //
    m_Opcodes[0x48] = [this]() -> int16_t {
        m_Memory[MEM_Stack + m_Registers.S] = m_Registers.A;
        --m_Registers.S;
        m_Cycles += 3;
        return 0;
    };

    //
    // PHP
    //
    m_Opcodes[0x08] = [this]() -> int16_t {
        m_Memory[MEM_Stack + m_Registers.S] = m_Registers.P | ( 1 << BIT_Break ) | ( 1 << BIT_AlwaysOne );
        --m_Registers.S;
        m_Cycles += 3;
        return 0;
    };

    //
    // PLA
    //
    m_Opcodes[0x68] = [this]() -> int16_t {
        ++m_Registers.S;
        m_Registers.A = m_Memory[MEM_Stack + m_Registers.S];
        status_SetZero( m_Registers.A );
        status_SetNegative( m_Registers.A );
        m_Cycles += 4;
        return 0;
    };

    //
    // PLP
    //
    m_Opcodes[0x28] = [this]() -> int16_t {
        ++m_Registers.S;
        m_Registers.P = m_Memory[MEM_Stack + m_Registers.S] & ~( 1 << BIT_Break );
        m_Cycles += 4;
        return 0;
    };


    //
    // Jumps
    //

    //
    // JMP (Absolute)
    //
    m_Opcodes[0x4C] = [this]() -> int16_t {
        m_Registers.PC = absolute();
        m_Cycles += 3;
        return 0;
    };

    //
    // JMP (Indirect)
    //
    m_Opcodes[0x6C] = [this]() -> int16_t {
        m_Registers.PC = indirect();
        m_Cycles += 5;
        return 0;
    };

    //
    // JSR (Absolute)
    //
    m_Opcodes[0x20] = [this]() -> int16_t {
        WriteWord( MEM_Stack + m_Registers.S - 1, m_Registers.PC + 1 );
        m_Registers.S -= 2;
        m_Registers.PC = absolute();
        m_Cycles += 6;
        return 0;
    };

    //
    // RTI
    //
    m_Opcodes[0x40] = [this]() -> int16_t {
        m_Registers.P = m_Memory[MEM_Stack + m_Registers.S + 1] | ( 1 << BIT_AlwaysOne );
        ++m_Registers.S;
        m_Registers.PC = MAKE_WORD( m_Memory + MEM_Stack + m_Registers.S + 1 );
        m_Registers.S += 2;
        m_Cycles += 6;
        return 0;
    };

    //
    // RTS
    //
    m_Opcodes[0x60] = [this]() -> int16_t {
        const uint16_t stackPos = m_Registers.S + 1;
        m_Registers.PC = MAKE_WORD( m_Memory + MEM_Stack + stackPos ) + 1;
        m_Registers.S += 2;
        m_Cycles += 6;
        return 0;
    };

    //
    // Misc
    //

    //
    // NOP
    //
    m_Opcodes[0xEA] = [this]() -> int16_t {
        // Do nothing!
        m_Cycles += 2;
        return 0;
    };

    //
    // BRK
    //
    m_Opcodes[0x00] = [this]() -> int16_t {
        // Write the return address
        WriteWord( MEM_Stack + m_Registers.S - 1, m_Registers.PC + 1 );
        m_Registers.S -= 2;

        // Write the status
        m_Memory[MEM_Stack + m_Registers.S] = m_Registers.P | ( 1 << BIT_Break ) | ( 1 << BIT_AlwaysOne );
        --m_Registers.S;

        // Flag the interrupt
        SET_BIT( m_Registers.P, BIT_Interrupt, true );

        // Jump
        m_Registers.PC = ReadWord( MEM_BRK_Vector );

        gLogger->Log( "[BRK: %d]\n", m_Cycles );

        m_Cycles += 7;
        return 0;
    };
}


//
//
//

uint8_t CPU::ReadByte( uint16_t addr ) const
{
    // Pass to the mapper
    if ( addr >= 0x6000 )
        return gMapper->PRG_ReadByte( addr );

    const uint16_t tAddr = TranslateAddress( addr );

    if ( tAddr == MEM_PPU_Status )
        return gPPU->ReadStatus();

    if ( tAddr == MEM_PPU_Control )
        return gPPU->ReadControl();

    if ( tAddr == MEM_PPU_Mask )
        return gPPU->ReadMask();

    if ( tAddr == MEM_PPU_Data )
        return gPPU->ReadData();

    if ( tAddr == MEM_PPU_OAM_Data )
        return gPPU->OAM_ReadData();

    if ( tAddr == MEM_Controller_One )
        return gController->ReadPlayer( 0 );

    if ( tAddr == MEM_Controller_Two )
        return gController->ReadPlayer( 1 );

    if ( gSoundEnabled && tAddr == gNESAPU->status_addr )
        return gNESAPU->read_status( _CPU_TimeElapsed() );

    return m_Memory[tAddr];
}

//
//
//

void CPU::WriteByte( uint16_t addr, uint8_t data )
{
    if ( addr >= 0x6000 )
    {
        gMapper->PRG_WriteByte( addr, data );
        return;
    }

    const uint16_t tAddr = TranslateAddress( addr );

    if ( tAddr == MEM_PPU_Address )
    {
        gPPU->WriteAddress( data );
        return;
    }

    if ( tAddr == MEM_PPU_Control )
    {
        gPPU->WriteControl( data );
        return;
    }

    if ( tAddr == MEM_PPU_Mask )
    {
        gPPU->WriteMask( data );
        return;
    }

    if ( tAddr == MEM_PPU_Scroll )
    {
        gPPU->WriteScroll( data );
        return;
    }

    if ( tAddr == MEM_PPU_Data )
    {
        gPPU->WriteData( data );
        return;
    }

    if ( tAddr == MEM_Controller_One )
    {
        gController->WritePlayer( 0, data );
        return;
    }

    if ( tAddr == MEM_Controller_Two )
    {
        gController->WritePlayer( 1, data );
        return;
    }

    if ( tAddr == MEM_PPU_OAM_Address )
    {
        gPPU->OAM_WriteAddress( data );
        return;
    }

    if ( tAddr == MEM_PPU_OAM_Data )
    {
        gPPU->OAM_WriteData( data );
        return;
    }

    if ( tAddr == MEM_PPU_OAM_DMA )
    {
        gPPU->OAM_DMA( data );
        return;
    }

    // FIXME: How is this causing the background to pulsate??
    if ( gSoundEnabled )
    {
        if ( tAddr >= gNESAPU->start_addr && tAddr <= gNESAPU->end_addr )
        {
            gNESAPU->write_register( _CPU_TimeElapsed(), tAddr, data );
            return;
        }
    }

    m_Memory[tAddr] = data;
}


//
// System reset
//

void CPU::Reset( const uint16_t overrideAddr /* = MEM_Reset_Vector */ )
{
    // Put us into a known state
    m_Cycles = 0;
    m_Registers.init();
    m_NMIPending = 0;

    RandomizeMemory();

    // If we're overriding the start address, just take it literally
    if ( overrideAddr != MEM_Reset_Vector )
    {
        m_Registers.PC = overrideAddr;
    }
    else
    {
        m_Registers.PC = ReadWord( overrideAddr );
    }
}


//
// Non-maskable interrupt
//

void CPU::ExecuteNMI()
{
    // Write the return address
    WriteWord( MEM_Stack + m_Registers.S - 1, m_Registers.PC );
    m_Registers.S -= 2;

    // Write the status
    m_Memory[MEM_Stack + m_Registers.S] = m_Registers.P;
    --m_Registers.S;

    // Jump
    m_Registers.PC = ReadWord( MEM_NMI_Vector );

    gLogger->Log( "[NMI: %d]\n", m_Cycles );

    m_Cycles += 7;
}


//
// IRQ
//

void CPU::ExecuteIRQ()
{
    // Write the return address
    WriteWord( MEM_Stack + m_Registers.S - 1, m_Registers.PC );
    m_Registers.S -= 2;

    // Write the status
    m_Memory[MEM_Stack + m_Registers.S] = m_Registers.P;
    --m_Registers.S;

    // Jump
    m_Registers.PC = ReadWord( MEM_IRQ_Vector );

    gLogger->Log( "[IRQ: %d]\n", m_Cycles );

    m_Cycles += 7;
}


//
//
//

inline size_t _WriteStatus( char *target, uint8_t status )
{
    if ( status & ( 1 << BIT_Negative ) )
        target[0] = 'N';
    else
        target[0] = 'n';

    if ( status & ( 1 << BIT_Overflow ) )
        target[1] = 'V';
    else
        target[1] = 'v';

    if ( status & ( 1 << BIT_AlwaysOne ) )
        target[2] = '*';
    else
        target[2] = '-';

    if ( status & ( 1 << BIT_Break ) )
        target[3] = '*';
    else
        target[3] = '-';

    if ( status & ( 1 << BIT_Decimal ) )
        target[4] = 'D';
    else
        target[4] = 'd';

    if ( status & ( 1 << BIT_Interrupt ) )
        target[5] = 'I';
    else
        target[5] = 'i';

    if ( status & ( 1 << BIT_Zero ) )
        target[6] = 'Z';
    else
        target[6] = 'z';

    if ( status & ( 1 << BIT_Carry ) )
        target[7] = 'C';
    else
        target[7] = 'c';

    return 8;
}

inline size_t _WriteByte( char *target, uint8_t data )
{
    // Upper nibble
    const uint8_t upperNibble = ( data >> 4 );
    if ( upperNibble < 10 )
        target[0] = 48 + upperNibble;
    else
        target[0] = 65 + ( upperNibble - 10 );

    const uint8_t lowerNibble = ( data & 0x0F );
    if ( lowerNibble < 10 )
        target[1] = 48 + lowerNibble;
    else
        target[1] = 65 + ( lowerNibble - 10 );

    return 2;
}

inline size_t _WriteWord( char *target, uint16_t data )
{
    //
    _WriteByte( target, ( uint8_t )( data >> 8 ) );
    _WriteByte( target + 2, ( uint8_t )( data & 0x00FF ) );

    return 4;
}

inline int _NumDigits( uint64_t data )
{
    if ( data == 0 )
        return 1;

    if ( data > 0 )
        return (int)log10( data ) + 1;

    return (int)log10( abs( (long long)data ) ) + 2; // Accounting for negative sign
}

inline size_t _WritePaddedInt( char *target, int padAmount, int data )
{
    size_t charsWritten = 0;
    const int numDigits = _NumDigits( data );
    int padding = ( padAmount - numDigits );
    while ( padding > 0 )
    {
        target[charsWritten] = '0';
        ++charsWritten;
        --padding;
    }

    const size_t k_MaxNumbers = 11; // With negative sign
    _itoa_s( data, target + charsWritten, k_MaxNumbers, 10 );

    return max( padAmount, numDigits );
}

inline size_t _WriteLLU( char *target, uint64_t data )
{
    const size_t k_MaxNumbers = 11;
    _ltoa_s( (long)data, target, k_MaxNumbers, 10 );

    return _NumDigits( data );
}


//
//
//

void CPU::LogCycle()
{
    if ( !gLogger->IsLogging() )
        return;

    char entry[256];
    char *target = entry;

    target += _WriteWord( target, m_Registers.PC - 0x01 );
    *( target++ ) = '\t';

    const uint8_t opCode = ReadByte( m_Registers.PC - 0x01 );
    target += _WriteByte( target, opCode );
    *( target++ ) = ' ';

    const auto itr = m_OpCodeLookup.find( opCode );
    size_t length = strlen( itr->second );
    memcpy( target, itr->second, length );
    target += length;
    *( target++ ) = '\t';

    *( target++ ) = 'A';
    *( target++ ) = ':';
    *( target++ ) = ' ';
    target += _WriteByte( target, m_Registers.A );
    *( target++ ) = ' ';

    *( target++ ) = 'X';
    *( target++ ) = ':';
    *( target++ ) = ' ';
    target += _WriteByte( target, m_Registers.X );
    *( target++ ) = ' ';

    *( target++ ) = 'Y';
    *( target++ ) = ':';
    *( target++ ) = ' ';
    target += _WriteByte( target, m_Registers.Y );
    *( target++ ) = ' ';

    *( target++ ) = 'S';
    *( target++ ) = 'P';
    *( target++ ) = ':';
    *( target++ ) = ' ';
    target += _WriteByte( target, m_Registers.S );
    *( target++ ) = ' ';

    *( target++ ) = 'P';
    *( target++ ) = ':';
    *( target++ ) = ' ';
    target += _WriteStatus( target, m_Registers.P );
    *( target++ ) = '\t';

    *( target++ ) = 'P';
    *( target++ ) = 'P';
    *( target++ ) = 'U';
    *( target++ ) = ':';
    *( target++ ) = ' ';
    target += _WritePaddedInt( target, 3, _PPU_GetCycle() );

    *( target++ ) = ',';
    *( target++ ) = ' ';
    target += _WritePaddedInt( target, 3, _PPU_GetScanline() );
    *( target++ ) = '\t';

    *( target++ ) = 'C';
    *( target++ ) = 'Y';
    *( target++ ) = 'C';
    *( target++ ) = ':';
    *( target++ ) = ' ';
    target += _WriteLLU( target, m_Cycles );

    // End of line
    *( target++ ) = '\n';

    // NULL terminate
    *target = 0;

    gLogger->LogString( entry );
}


//
//
//

uint8_t CPU::Run()
{
    const uint64_t cyclesThisRun = m_Cycles;

    // Any pending NMIs should be handled here
    // NOTE: The NMI must happen after the next instruction finishes, so we delay one
    //       frame via counting down the signal value
    if ( m_NMIPending > 0 )
    {
        --m_NMIPending;

        if ( !m_NMIPending )
        {
            if ( gPPU->IsVBlankFlagged() )
            {
                ExecuteNMI();
                return static_cast<uint8_t>( m_Cycles - cyclesThisRun );
            }
        }
    }

    if ( m_IRQPending )
    {
        m_IRQPending = 0;
        ExecuteIRQ();
        return static_cast<uint8_t>( m_Cycles - cyclesThisRun );
    }

    uint64_t startCycles = m_Cycles;
    const uint8_t opCode = ReadByte( m_Registers.PC );
    ++m_Registers.PC;
    const auto fnOpCode = m_Opcodes[opCode];
    if ( !fnOpCode )
    {
        ASSERT( 0 );
        char errorText[256];
        snprintf( errorText, sizeof( errorText ), "Illegal instruction: 0x%02X at 0x%04X\n", opCode, m_Registers.PC );
        gLogger->LogString( errorText );
        _SignalExit( errorText );
        ++m_Registers.PC;
        return 1;
    }

    LogCycle();

    // Run the opcode and move the PC forward by the returned offset
    m_Registers.PC += fnOpCode();

    return static_cast<uint8_t>( m_Cycles - cyclesThisRun );
}
