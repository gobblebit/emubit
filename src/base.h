#ifndef BASE_H
#define BASE_H

#include <windows.h>

#include <cassert>
#define ASSERT assert

#include <cstdint> // uintXX_t

#include "memory_maps.h"
#include "logger.h"

enum class MirrorMode
{
    Horizontal,
    Vertical,
    Single_A,
    Single_B,
    All
};

inline void SET_BIT( uint8_t &target, uint8_t bit, bool state )
{
    target = ( target & ~( 1 << bit ) ) | ( state << bit );
}

// #define OUTPUT_NTSC
// #define OUTPUT_RGBA
#define OUTPUT_RGB
// #define OUTPUT_16BIT

const uint16_t gNativeWidth = 256;
const uint16_t gNativeHeight = 240;

const uint64_t k_CPU_StartOffset = 0x00;
const int k_ScanlinesPerFrame = 262;                                    // PPU
const int k_ScanlinesBeforeVBlank = 241;                                // PPU
const int k_CyclesPerScanline = 341;                                    // PPU
const int k_CyclesPerFrame = k_CyclesPerScanline * k_ScanlinesPerFrame; // PPU
const int k_PixelsPerCycle = 1;                                         // PPU
const int k_PPUCyclesPerCPU = 3;                                        // CPU

const int k_CPUCyclesPerScanline = ( k_CyclesPerScanline / k_PPUCyclesPerCPU );

class Logger;
extern Logger *gLogger;

class CPU;
extern CPU *gCPU;

class PPU;
extern PPU *gPPU;

class Mapper;
extern Mapper *gMapper;

class Controller;
extern Controller *gController;

class DebugOverlay;
extern DebugOverlay *gDebugOverlay;

struct SDL_Renderer;
extern SDL_Renderer *gRenderer;

class Nes_Apu;
extern Nes_Apu *gNESAPU;

#endif // BASE_H