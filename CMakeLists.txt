cmake_minimum_required (VERSION 3.13)
cmake_policy(VERSION 3.13)

option( CMAKE_GENERATOR_PLATFORM "X64" X64 )

set( CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/cmake/" )

project ( emubit
    VERSION 0.9.1
    DESCRIPTION "EmuBit"
    LANGUAGES CXX
)

# Include SDL2
list(APPEND CMAKE_PREFIX_PATH "../../thirdparty/SDL2-2.26.0")
find_package(SDL2 REQUIRED)
include_directories( emubit ${SDL2_INCLUDE_DIRS})

set( SOURCE
    #Core
    "src/apu.cpp"
    "src/apu.h"
    "src/base.h"
    "src/cart.h"
    "src/controller.cpp"
    "src/controller.h"
    "src/cpu.cpp"
    "src/cpu.h"
    "src/debug_overlay.cpp"
    "src/debug_overlay.h"
    "src/main.cpp"
    "src/mapper.h"
    "src/mapper_87.h"
    "src/mapper_axrom.h"
    "src/mapper_cnrom.h"
    "src/mapper_mmc1.h"
    "src/mapper_mmc2.h"
    "src/mapper_mmc3.h"
    "src/mapper_nrom.h"
    "src/mapper_unrom.h"
    "src/memory_maps.h"
    "src/ppu.cpp"
    "src/ppu.h"
    "src/shift_register.h"
    "src/testing.cpp"
    "src/testing.h"
    "src/timer.h"

    #NES APU
    "src/thirdparty/nes_snd_emu/Simple_Apu.cpp"
    "src/thirdparty/nes_snd_emu/Sound_Queue.cpp"
    "src/thirdparty/nes_snd_emu/nes_apu/apu_snapshot.cpp"
    "src/thirdparty/nes_snd_emu/nes_apu/Blip_Buffer.cpp"
    "src/thirdparty/nes_snd_emu/nes_apu/Multi_Buffer.cpp"
    "src/thirdparty/nes_snd_emu/nes_apu/Nes_Apu.cpp"
    "src/thirdparty/nes_snd_emu/nes_apu/Nes_Namco.cpp"
    "src/thirdparty/nes_snd_emu/nes_apu/Nes_Oscs.cpp"
    "src/thirdparty/nes_snd_emu/nes_apu/Nes_Vrc6.cpp"
    "src/thirdparty/nes_snd_emu/nes_apu/Nonlinear_Buffer.cpp"

    #NES Video Filter
    "src/thirdparty/nes_ntsc/nes_ntsc.cpp"
    "src/thirdparty/nes_ntsc/nes_ntsc.h"
    "src/thirdparty/nes_ntsc/nes_ntsc_config.h"
    "src/thirdparty/nes_ntsc/nes_ntsc_impl.h"
)

add_subdirectory( src/thirdparty )

set( INCLUDE_DIRECTORIES
    "src/thirdparty/SDL2"
    "src/thirdparty/nes_snd_emu"
)

add_executable(
    emubit
    WIN32
    ${SOURCE}
 )

target_link_libraries( emubit "${SDL2_LIBRARIES}" )

source_group( "Source" FILES ${SOURCE} )

set_target_properties( emubit
    PROPERTIES
    RUNTIME_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/../bin"
    CXX_EXTENSIONS OFF
)

target_compile_features(emubit PUBLIC cxx_std_17)

target_include_directories( emubit PRIVATE ${INCLUDE_DIRECTORIES} )

target_compile_options( emubit PRIVATE /Zp8 )

option( TESTING "Turn on the testing harness for automated unit testing." OFF )

if ( TESTING )
    add_definitions( -D_TESTING )
endif ( TESTING )
