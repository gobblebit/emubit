# emubit
NES emulator

## Features
* Cycle-accurate CPU emulation of the 6502 chipset
* PPU with proper *t* and *v* address register behavior
* Supported mappers
    - UNROM
    - CNROM
    - AxROM
    - GxROM
    - MMC1
    - MMC3

## Known Issues
* Mike Tyson's Punch-Out!! and Bubble Bobble have odd palette corruption that correspondes to sound.
* Sprite 0 issues on some titles (minor)

## TODO 
* Debugging helpers
* MMC5

## Game Support List
| Game | Supported | Issues |
|---|---|---|
|Balloon Fight|FULL||
|Excitebike|FULL||
|Ice Climber|FULL||
|Kung Fu|FULL||
|Super Mario Bros|FULL||
|Paperboy|FULL||
|Donkey Kong|FULL||
|Donkey Kong Classic|FULL||
|Arkanoid|FULL||
|Adventure Island Classic|FULL||
|Goonies|FULL||
|Goonies 2|FULL||
|Solomon's Key|FULL||
|Gradius|FULL||
|Jaws|FULL||
|Castlevania|FULL||
|Contra|FULL||
|Rygar|FULL||
|Ghost 'n Goblins|FULL||
|Metal Gear|FULL||
|Life Force|FULL||
|Blaster Master|FULL||
|Castlevania 2|FULL||
|Dr Mario|FULL||
|Metroid|FULL||
|Mega Man 2|FULL||
|Final Fantasy|FULL||
|Final Fantasy 3|FULL||
|Kid Icarus|FULL||
|Bionic Commando|FULL||
|Dragon Warrior|FULL||
|Ninja Gaiden|FULL||
|Boy and His Blob|FULL||
|RC Pro-Am|FULL||
|Bases Loaded|FULL||
|Golgo 13|FULL||
|Tetris|FULL||
|Blades of Steel|FULL||
|The Legend of Zelda|FULL||
|The Legend of Zelda 2|FULL||
|Journey to Silius|FULL||
|Qix|FULL||
|Rad Racer|FULL||
|Faxanadu|FULL||
|Willow|FULL||
|Bubble Bobble|FULL||
|Mike Tyson's Punch-Out!!|FULL||
|River City Ransom|FULL||
|Karnov|FULL||
|Mega Man|FULL||
|Mega Man 3|FULL||
|Mega Man 4|FULL||
|Mega Man 5|FULL||
|Metal Storm|FULL||
|Smash TV|FULL||
|Super Mario Bros 2|FULL||
|Super Mario Bros 3|FULL||
|Power Blade|FULL||
|Top Gun|FULL||
|Final Fantasy 2|FULL||
|Ikari Warriors|FULL||
|Swords and Serpents|FULL||
|Snake Rattle'n Roll|FULL||
|Jackal|FULL||
|Legendary Wings|FULL||
|Milan's Secret Castle|FULL||
|Bump'n'Jump|FULL||
|Friday the 13th|FULL||
|RBI Baseball|FULL||
|Super C|FULL||
|Kirby's Adventure|FULL||
|Ninja Gaiden 2|GOOD|Slight Sprite 0 offset|
|Rush'n Attack|GOOD|Status bar split|
|Section Z|GOOD|Sprite 0 split|
|Micro Machines|GOOD|Sprite 0 split|
|Duck Tales|GOOD|Sprite 0 tearing|
|Wizards & Warriors|GOOD|Minor status bar issue on inital screen|
|Teenage Mutant Ninja Turtles|GOOD|Some flickering|
|Maniac Mansion|GOOD|Sprite 0 issue in intro|
|Guardian Legend|GOOD|Status bar issues with lots of sprites|
|Double Dragon|GOOD|Slight status bar issue in opening intro|
|Commando|GOOD|This game is just buggy|
|Californai Games|GOOD|Some Sprite 0 issues|
|Battletoads|POOR|Jitter and timing issues|
|Adventures of Bayou Billy|PARTIAL|No zapper support!|
|Gumshoe|PARTIAL|Zapper not supported|
|Gyromite|PARTIAL|Controller 2 / ROB not supported|
|Kabuki - Quantum Fighter|NONE|Instant crash|